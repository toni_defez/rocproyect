﻿using System;
using Android.Content;
using Android.Preferences;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Droid.Utils
{
	public class UserCredentials
    {
        //guardo las credenciales del usuario para el proximo
        //logeo
        public static bool SaveUserLogin(UserNode user, Context context)
        {
            ISharedPreferences preferences =
                PreferenceManager.GetDefaultSharedPreferences(context);
            var json = JSON.SerializeJSON(user);

            if (preferences != null)
            {
                ISharedPreferencesEditor editor = preferences.Edit();
                editor.PutString("CREDENTIALS_USER", json);
                return editor.Commit();
            }

            return false;
        }

        /// <summary>
        /// Obtiene el usuario guardado en el PreferenceManager
        /// </summary>
        /// <returns>The user login.</returns>
        /// <param name="context">Context.</param>
		public static UserNode GetUserLogin(Context context)
        {
            ISharedPreferences sharedPrefs =
                PreferenceManager.GetDefaultSharedPreferences(context);
			UserNode user = null;

            try
            {
                user = JSON.DeserializeUsuarioLoginJSON(
                    sharedPrefs.GetString("CREDENTIALS_USER", null));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return user;
        }

        /// <summary>
        /// Hases the credentials saved.
        /// </summary>
        /// <returns><c>true</c>, if credentials saved was hased,
        ///  <c>false</c> otherwise.</returns>
        /// <param name="context">Context.</param>
        public static bool HasCredentialsSaved(Context context)
        {
            return (GetUserLogin(context) != null);
        }

        public static void Remove(Context context)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            ISharedPreferencesEditor editor = prefs.Edit();

            editor.Remove("CREDENTIALS_USER");

            editor.Commit();
        }

    }
}
