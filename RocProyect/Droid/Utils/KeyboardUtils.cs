﻿
using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;


namespace RocProyect.Droid.Utils
{
	public class KeyboardUtils
    {
        public static void HideKeyboard(Activity context)
        {
            var imm = (InputMethodManager)context.GetSystemService(Context.InputMethodService);
            int sdk = (int)Build.VERSION.SdkInt;

            if (sdk < 11)
            {
                imm.HideSoftInputFromWindow(context.Window.CurrentFocus.WindowToken, 0);
            }
            else
            {
                imm.HideSoftInputFromWindow(context.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
            }
        }
    }
}
