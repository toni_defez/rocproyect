﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Android.App;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Models;

namespace RocProyect.Droid.Adapters
{
	public class NotificacionAdapter:BaseAdapter<Chat>
    {

		private List<Chat> _items;
        private Activity _context;


        public List<Chat> Items { get => _items; set => _items = value; }
        public Activity Context { get => _context; set => _context = value; }

        public NotificacionAdapter()
        {
        }

		public NotificacionAdapter(List<Chat> items, Activity context)
		{
			Items = items;
			Context = context;
		}

		public override Chat this[int position]
        {
            get
            {
                return Items[position];
            }
        }

		public override int Count => Items.Count;


		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			Contract.Ensures(Contract.Result<View>() != null);
			Chat item = Items[position];
            View view = convertView;

            view = Context.LayoutInflater.Inflate(
				Resource.Layout.Notificaciones_CardView, null);
		
            //declarando objetos
            TextView mensaje = (TextView)view.FindViewById(Resource.Id.textoNotificacion);
            TextView usuario = (TextView)view.FindViewById(Resource.Id.nombre_User1);
            ImageView fotoUsuario = (ImageView)view.FindViewById(Resource.Id.imagen_User1);
			TextView fecha = (TextView)view.FindViewById(Resource.Id.fecha);
			//rellenando objetos

			mensaje.Text = item.UltimoMensaje;
			usuario.Text = item.NombreUsuario;
			fecha.Text = item.Fecha.ToShortDateString();
           
			Glide.With(_context).Load(item.FotoUsuario)
                .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform())
			     .Into(fotoUsuario);


			return view;
		}
	}
}
