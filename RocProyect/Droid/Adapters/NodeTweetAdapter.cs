﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Android.App;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Fragments;
using RocProyect.Models;
using RocProyect.Services;

namespace RocProyect.Droid.Adapters
{
	public class NodeTweetAdapter : BaseAdapter<NodeTweet>
	{

		private List<NodeTweet> _items;
		private Activity _context;
		public List<NodeTweet> Items { get => _items; set => _items = value; }
		public Activity Context { get => _context; set => _context = value; }
		public ITweetService _tweetService;
		private TweetProgramadosFragment _fragment; 

      
		public NodeTweetAdapter(List<NodeTweet> items, Activity context, TweetProgramadosFragment fragment)
		{
			Items = items;
			Context = context;
			_fragment = fragment;
		}


		public override NodeTweet this[int position]
		{
			get
			{
				return Items[position];
			}
		}


		public override int Count => Items.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public void RemoveItemAtPosition(int position)
        {
            _items.RemoveAt(position);
        }



		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			Contract.Ensures(Contract.Result<View>() != null);
			var item = Items[position];
			View view = convertView;

			view = Context.LayoutInflater.Inflate(
				Resource.Layout.NodeTweet_CardView, null);

			TextView contenido = (TextView)view.FindViewById(Resource.Id.textTweet);
			TextView hora = (TextView)view.FindViewById(Resource.Id.txtHora);
			TextView fecha = (TextView)view.FindViewById(Resource.Id.txtFecha);
			LinearLayout linearBorrar = (LinearLayout)view.FindViewById(Resource.Id.linearBorrar);
			LinearLayout linearEditar = (LinearLayout)view.FindViewById(Resource.Id.linearEditar);


			contenido.Text = item.text;
			hora.Text = item.hora;
			fecha.Text = item.fecha;

			linearEditar.Click +=delegate {

				RocActivity tmp = (RocActivity)_context;

				CrearTweetFragment fragment = new CrearTweetFragment();
				fragment.TweetModicar = item;
				tmp.SupportFragmentManager.BeginTransaction().
                Replace(Resource.Id.content_main, fragment)
                                     .Commit();            
			};



			linearBorrar.Click +=delegate {
				AlertDialog alertDialog = null;
				AlertDialog.Builder builder = new AlertDialog.Builder(_context);
                builder.SetTitle("Borrar tweet");
                builder.SetMessage("¿Estas seguro de borrar el tweet programado?");
                builder.SetCancelable(false);
				builder.SetPositiveButton("Si", async delegate {

					_tweetService = new TweetService();
					var result = await _tweetService.GetDeleteTweetAsync(Constants.userNode.Token,
																   item._id);
					if(result.ok==true)
					{
						RemoveItemAtPosition(position);
                        _context.RunOnUiThread(() => this.NotifyDataSetChanged());
						 _fragment.ActualizarListasAsync();

					}
				});
				builder.SetNegativeButton("No", delegate
                {
					alertDialog.Dismiss();
                });

				alertDialog = builder.Create();
                alertDialog.Show();
			};



			return view;
		}
	}
}
