﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Droid.Adapters
{
	public class FollowersBusquedaAdapter : BaseAdapter<Follower>
	{

		private List<Follower> _items;
		private Activity _context;
		public List<Follower> Items { get => _items; set => _items = value; }
		public Activity Context { get => _context; set => _context = value; }
		public override int Count => Items.Count;

		public override Follower this[int position] => throw new NotImplementedException();

		private Twitter _twitter;

		public FollowersBusquedaAdapter(List<Follower> items, Activity context)
		{
			Items = items;
			Context = context;
			_twitter = Twitter.getInstance;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			Contract.Ensures(Contract.Result<View>() != null);
			var item = Items[position];
			View view = convertView;

			view = Context.LayoutInflater.Inflate(
				Resource.Layout.Usuarios_CardView, null);

			ImageView imagenUser = (ImageView)view.FindViewById(Resource.Id.imagen_User1);
			TextView nombreUser = (TextView)view.FindViewById(Resource.Id.nombre_User1);
			TextView estadoUser = (TextView)view.FindViewById(Resource.Id.txt_tweet1);
			Button btnMensaje = (Button)view.FindViewById(Resource.Id.btnMensaje);
			Button btnFollow = (Button)view.FindViewById(Resource.Id.btnSeguir);
			_twitter = Twitter.getInstance;

			btnMensaje.Click += delegate
			{
				MostrarDialgoInput(item);
			};

			btnFollow.Click += delegate
			{

				bool result = _twitter.EmpezarASeguirUsuario(item.id_follower);
				if (result)
					Toast.MakeText(_context, "Has empezado a seguir " + item.Nombre, ToastLength.Long);
			};
           
			//rellenando campos
			Glide.With(_context).Load(item.Imagen)
			       .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform()
			       .Placeholder(Resource.Mipmap.load).Error(Resource.Mipmap.cancel))
			       .Into(imagenUser);
			nombreUser.Text = item.Nombre;
			estadoUser.Text = item.estado;
                     
			imagenUser.Click += delegate {
				Follower tmp = item;
				Intent intent = new Intent(Context, typeof(DetalleFollower));
                intent.PutExtra("detalleFollower",
                        JSON.SerializeJSON(tmp));
				Context.StartActivityForResult(intent, 1);
				Context.OverridePendingTransition(Resource.Animation.slide_left, Resource.Animation.fade_out);
                
			};
                     
			return view;

		}

		private void MostrarDialgoInput(Follower item)
		{
			LayoutInflater layoutInflater = LayoutInflater.From(_context);
			View mView = layoutInflater.Inflate(Resource.Layout.AlertDialogEditText, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(_context);
			builder.SetView(mView);

			EditText email = mView.FindViewById<EditText>(Resource.Id.campoMensaje);



			AlertDialog alertDialog = null;
			builder.SetCancelable(false).SetPositiveButton("Enviar", async delegate
							 {
								 if (email.Length() != 0)
									 await _twitter.EnviarMensajeAsync(email.Text, item.id_follower);
							 });
			builder.SetNegativeButton("Cerrar", delegate
			{
				alertDialog.Dismiss();
			});

			alertDialog = builder.Create();
			alertDialog.Show();

		}

	}
}
