﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Models;
using RocProyect.Models;

namespace RocProyect.Droid.Adapters
{
	public class MensajeAdapter : RecyclerView.Adapter
	{
		private const  int VIEW_TYPE_MESSAGE_SENT = 1;
		private const int VIEW_TYPE_MESSAGE_RECEIVED = 2;

		private List<Mensaje> _items;
		private Activity _context;
		private Twitter _twitter;

		public List<Mensaje> Items { get => _items; set => _items = value; }
		public Activity Context { get => _context; set => _context = value; }


		public override int ItemCount => Items.Count;


		public MensajeAdapter()
		{
		}

		public MensajeAdapter(List<Mensaje> items, Activity context)
		{
			Items = items;
			Context = context;
		}

		public override int GetItemViewType(int position)
		{
			Mensaje message = (Mensaje)Items[position];
			long userId = Twitter.getInstance.user.Id;

			if (message.IdSender1 == userId)
				return VIEW_TYPE_MESSAGE_SENT;
			else
				return VIEW_TYPE_MESSAGE_RECEIVED;

		}


		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			Mensaje item = Items[position];
			switch (this.GetItemViewType(position)) 
			{
				case VIEW_TYPE_MESSAGE_SENT:
					((MensajeEnviadoHolder)holder).bind(item);
					break;
				case VIEW_TYPE_MESSAGE_RECEIVED:
					((MensajeRecibidosHolder)holder).bind(item);
					break;
			}
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			View view;
			if (viewType == VIEW_TYPE_MESSAGE_SENT)
			{
				LayoutInflater inflater = LayoutInflater.From(parent.Context);
				view = inflater.Inflate(Resource.Layout.mensaje_enviado,
                                             parent, false);
				return new MensajeEnviadoHolder(view);
			}
			//else (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
			else{
				LayoutInflater inflater = LayoutInflater.From(parent.Context);
				view = inflater.Inflate(Resource.Layout.mensaje_recibido,
                                             parent, false);
				return new MensajeRecibidosHolder(view,_context);
			}


		}
	}



	//Holder para los mensajes enviados
	public class MensajeEnviadoHolder : RecyclerView.ViewHolder
	{
		TextView messageText, timeText;

		public MensajeEnviadoHolder(View itemView) : base(itemView)
		{
			messageText = (TextView)itemView.FindViewById(Resource.Id.text_message_body);
			timeText = (TextView)itemView.FindViewById(Resource.Id.text_message_time);
		}

		public void bind(Mensaje message)
		{
			messageText.Text = message.Texto;
			timeText.Text = message.Fecha.ToShortDateString();

		}
	}

	//Holder para los mensajes recibidos
	public class MensajeRecibidosHolder : RecyclerView.ViewHolder
	{

		TextView messageText, timeText, nameText;
	    ImageView profileImage;
		Activity _context;

		public MensajeRecibidosHolder(View itemView,Activity context) : base(itemView)
		{
			messageText = (TextView)itemView.FindViewById(Resource.Id.text_message_body);
			timeText = (TextView)itemView.FindViewById(Resource.Id.text_message_time);
			nameText = (TextView)itemView.FindViewById(Resource.Id.text_message_name);
			profileImage = (ImageView)itemView.FindViewById(Resource.Id.image_message_profile);
			_context = context;
		}

		public void bind(Mensaje message)
		{
			messageText.Text = message.Texto;
			timeText.Text = message.Fecha.ToShortDateString();

			nameText.Text = message.Sender.Nickname;

			Glide.With(_context).Load(message.Sender.ProfileUrl)
               .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform())
			     .Into(profileImage);         
		}
	}

}
