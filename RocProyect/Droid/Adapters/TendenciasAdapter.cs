﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;


namespace RocProyect.Droid.Adapters
{
	public class TendenciasAdapter:BaseAdapter<Tendencia>
    {

		private List<Tendencia> _items;
        private Activity _context;
		public List<Tendencia> Items { get => _items; set => _items = value; }
        public Activity Context { get => _context; set => _context = value; }
		public override int Count => Items.Count;
		private Twitter _twitter;

		public TendenciasAdapter()
        {
        }

		public TendenciasAdapter(List<Tendencia> items, Activity context)
        {
            Items = items;
            Context = context;
            _twitter = Twitter.getInstance;
        }
       

		public override Tendencia this[int position]
        {
            get
            {
                return Items[position];
            }
        }


		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			Contract.Ensures(Contract.Result<View>() != null);
            var item = Items[position];
            View view = convertView;

            view = Context.LayoutInflater.Inflate(
				Resource.Layout.Tendencia_CardView, null);

			TextView texto = (TextView)view.FindViewById(Resource.Id.TendenciaTweet);
			TextView dato1 = (TextView)view.FindViewById(Resource.Id.dato1);
			texto.Text = item.Nombre;
			if (item.Volumen != null)
			{
				dato1.Visibility = ViewStates.Visible;
				dato1.Text = "Volumen de tweets:"+ item.Volumen ;
			}
			else
				dato1.Visibility = ViewStates.Gone;
               

			return view;
		}
	}
}
