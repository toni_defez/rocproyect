﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Droid.Adapters
{
    public class TimeLineAdapter:BaseAdapter<MiTweet>
    {
        private List<MiTweet> _items;
        private Activity _context;
        public List<MiTweet> Items { get => _items; set => _items = value; }
        public Activity Context { get => _context; set => _context = value; }
        private Twitter _twitter;


        public TimeLineAdapter()
        {
        }

        public TimeLineAdapter(List<MiTweet> items, Activity context)
        {
            Items = items;
            Context = context;
            _twitter = Twitter.getInstance;
        }

        public override MiTweet this[int position] {
            get{
                return Items[position];
            }
        }

        public override int Count => Items.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            Contract.Ensures(Contract.Result<View>() != null);
            var item = Items[position];
            View view = convertView;

            view = Context.LayoutInflater.Inflate(
                Resource.Layout.Tweet_CardView, null);

            ImageView userImagen = (ImageView)view.FindViewById(Resource.Id.imagen_User);
            TextView nombreUser = (TextView)view.FindViewById(Resource.Id.nombre_User);
            TextView txtTwitter = (TextView)view.FindViewById(Resource.Id.txt_tweet);
            TextView txtDate = (TextView)view.FindViewById(Resource.Id.txt_fecha);
            ImageButton btnLike = (ImageButton)view.FindViewById(Resource.Id.btn_like);
            ImageButton btnReply = (ImageButton)view.FindViewById(Resource.Id.btn_reply);
            ImageButton btnReTweet = (ImageButton)view.FindViewById(Resource.Id.btn_retweet);
            ImageView imageTweet = (ImageView)view.FindViewById(Resource.Id.imageTweet);
			ImageButton btnMore = (ImageButton)view.FindViewById(Resource.Id.btn_more);

            if (item.favorito)
                btnLike.SetImageResource(Resource.Mipmap.like_lleno);
            else
                btnLike.SetImageResource(Resource.Mipmap.like_vacio);

            if (item.retweet)
                btnReTweet.SetImageResource(Resource.Mipmap.retweet_lleno);
            else
                btnReTweet.SetImageResource(Resource.Mipmap.retweet_vacio);

			if (item.contestado)
				btnReply.SetImageResource(Resource.Mipmap.reply_lleno);
			else
				btnReply.SetImageResource(Resource.Mipmap.reply_vacio);


            btnLike.Click+=delegate {
                _twitter.HacerFavorito(item);
                btnLike.SetImageResource(Resource.Mipmap.like_lleno);
                item.favorito = true;

            };

            //el boton de reply lo que hara sera abrir otra actividad
            //donde  podremos ver mas detalles del tweet
			btnMore.Click+=delegate {
            
                MiTweet tmpTweet = item;
				Intent intent = new Intent(this.Context, typeof(DetallesTweetActivity));
                intent.PutExtra("detallesTweet",
                                           JSON.SerializeJSON(tmpTweet));
                Context.StartActivityForResult(intent, 0);
				Context.OverridePendingTransition(Resource.Animation.slide_right, Resource.Animation.fade_out);
                
            };

			btnReply.Click+=delegate {

				MiTweet tmp = item;
				EnviarMensaje(item,btnReply);
			};

            btnReTweet.Click+=delegate {

                _twitter.HacerRetweet(item);
                btnReTweet.SetImageResource(Resource.Mipmap.retweet_lleno);
                item.retweet = true;
            };

            //poniendo valores 
            Glide.With(_context).Load(item.imagen_usuario)
                 .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform()
			            .Placeholder(Resource.Mipmap.load).Error(Resource.Mipmap.cancel))
                 .Into(userImagen);
            nombreUser.Text = item.nombre_usuario;
            txtTwitter.Text = item.text;
			txtDate.Text = item.time; 

            //poniendo Tweet con foto
            if(item.imagen_tweet!=null)
            {
                Glide.With(_context).Load(item.imagen_tweet)
				     .Apply(Com.Bumptech.Glide.Request.RequestOptions.FitCenterTransform()
				            .Placeholder(Resource.Mipmap.load).Error(Resource.Mipmap.cancel))
                     .Into(imageTweet);


				imageTweet.Click +=delegate {
				
					MostrarImagen(imageTweet.Drawable);
				}; 
            }
            else
            {
                imageTweet.Visibility = ViewStates.Gone;
            } 
            
            return view;
        }

		private void MostrarImagen(Drawable drawable)
		{
			LayoutInflater layoutInflater = LayoutInflater.From(_context);
			View mView = layoutInflater.Inflate(Resource.Layout.ImageViewZoomLayout , null);
			AlertDialog.Builder builder = new AlertDialog.Builder(_context,Resource.Style.full_screen_dialog);
            builder.SetView(mView);
			ImageView image = mView.FindViewById<ImageView>(Resource.Id.imageView);
			image.SetImageDrawable(drawable);
			AlertDialog alertDialog = null;
			builder.SetCancelable(false).SetPositiveButton("Cerrar",  delegate
			{
				alertDialog.Dismiss();
			});

			alertDialog = builder.Create();
			alertDialog.Show();

		}

		private void EnviarMensaje(MiTweet item, ImageButton btnReply)
        {
            LayoutInflater layoutInflater = LayoutInflater.From(_context);
            View mView = layoutInflater.Inflate(Resource.Layout.AlertDialogEditText, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(_context);
            builder.SetView(mView);

            EditText email = mView.FindViewById<EditText>(Resource.Id.campoMensaje);
                     
            AlertDialog alertDialog = null;
            builder.SetCancelable(false).SetPositiveButton("Send", async delegate
            {
				if (email.Length() != 0)
				{
					ProgressDialog tmp = new ProgressDialog(_context);
					tmp.SetMessage("Enviando respuesta...");
					tmp.Show();
					bool contestado= await _twitter.ContestarTweetAsync(item, email.Text);
                    
					if (contestado)
					{
						item.contestado = true;
						btnReply.SetImageResource(Resource.Mipmap.reply_lleno);
					}
					tmp.Dismiss();
				}
				
            });
            builder.SetNegativeButton("Cerrar", delegate
            {
                alertDialog.Dismiss();
            });

            alertDialog = builder.Create();
            alertDialog.Show();

        }
    }
}
