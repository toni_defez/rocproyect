﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System;
using System.Collections.Generic;
using Android.Support.V4.App;
using Android.Views;
using RocProyect.Droid.Fragments;

namespace RocProyect.Droid.Adapters
{

	public class FragmentPagerAdapter : FragmentStatePagerAdapter
	{
		readonly List<Fragment> fragmentList = new List<Fragment>();

		public FragmentPagerAdapter(Android.Support.V4.App.FragmentManager fragmentManager) : base(fragmentManager)
        {
        }

        public override int Count
        {
            get
            {
                return fragmentList.Count;
            }
        }

		public override Fragment GetItem(int position)
        {
            return fragmentList[position];
        }

        public void addFragmentView(Func<LayoutInflater, ViewGroup, Android.OS.Bundle, View> fragmentView)
        {
			fragmentList.Add(new SimpleTweetFragment(fragmentView));
        }

	}
}
