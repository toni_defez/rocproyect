﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Models;

namespace RocProyect.Droid.Adapters
{
    public class FollowerAdapter: RecyclerView.Adapter
    {
        private Activity _context;
        private List<Follower> lsData = new List<Follower>();
        public event EventHandler<int> ItemClick;
  

        public FollowerAdapter()
        {
        }

        public FollowerAdapter(Activity context, List<Follower> lsData)
        {
            _context = context;
            this.lsData = lsData;
        }

        public override int ItemCount => lsData.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            MyRecyclerViewHolder viewHolder = holder as MyRecyclerViewHolder;
            viewHolder.IsRecyclable = true;
            Follower item = lsData[position];

            Glide.With(_context).Load(item.Imagen)
                .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform()
			           .Placeholder(Resource.Mipmap.load).Error(Resource.Mipmap.cancel))
                 .Into(viewHolder.ImgPerfil);
            
            if(item.estado.Length>20)
            {
                string estado = item.estado.Substring(0, 10) + "...";
                viewHolder.TxtEstado.Text = estado;
            }
            else
                viewHolder.TxtEstado.Text = item.estado;
            viewHolder.TxtPerfil.Text = item.Nombre;

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.From(parent.Context);
            View itemView = inflater.Inflate(Resource.Layout.Followers_CardView,
                                             parent, false);
            return new MyRecyclerViewHolder(itemView, OnClick);
        }


        private void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }

    }



    public class MyRecyclerViewHolder : RecyclerView.ViewHolder
    {

        private ImageView _imgPerfil;
        private TextView _txtPerfil;
        private TextView _txtEstado;

        public ImageView ImgPerfil { get => _imgPerfil; set => _imgPerfil = value; }
        public TextView TxtPerfil { get => _txtPerfil; set => _txtPerfil = value; }
        public TextView TxtEstado { get => _txtEstado; set => _txtEstado = value; }


        public MyRecyclerViewHolder(View view, Action<int> listener) : base(view)
        {
            ImgPerfil = (ImageView)view.FindViewById(Resource.Id.imagen_User1);
            TxtPerfil = (TextView)view.FindViewById(Resource.Id.nombre_User1);
            TxtEstado = (TextView)view.FindViewById(Resource.Id.txt_tweet1);
        
            view.Click += (sender, e) => listener(base.Position);
        }

    
    }
}
