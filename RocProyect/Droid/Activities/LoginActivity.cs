﻿
using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using RocProyect.Droid.Utils;
using RocProyect.Models;
using RocProyect.Services;

namespace RocProyect.Droid.Activities
{

    /// <summary>
    /// Pantalla inicial de la aplicacion , en pantalla login podremos hacer 3 funciones
	/// logearnos, iniciar el proceso de registro y por ultimo recordar nuestra 
	/// contraseña si en algun momento la olvidamos
    /// </summary>
	[Activity(Label = "RocProyect", MainLauncher = true, Icon = "@mipmap/logo",
			  Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
	public class LoginActivity : AppCompatActivity
	{
		private Button _buttonLogin;
		private TextView _mail;
		private TextView _password;
		private TextView _linkRegister;
		private TextView _rememberPass;
		private TextView _linkRegister2;
		private IUserService _loginService;
              
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			_loginService = new UserService();

            //Comprobamos si el usuarios se ha guardado su sesion
			if (Utils.UserCredentials.HasCredentialsSaved(this))
			{
				//iniciamos el login automatico
				SetContentView(Resource.Layout.Splash);
				IniciarSesionGuardada();
			}
			else
			{
				//mostramos el login normal
				SetContentView(Resource.Layout.LoginActivity);
				IniciarComponentes();
				_mail.Text = "toni.defez@gmail.com";
				_password.Text = "soigenial123";
				IniciarBotones();            
			}         
		}

		private async void IniciarSesionGuardada()
		{
			ProgressDialog progress = new ProgressDialog(this);
			progress.SetMessage("Comprobando credenciales");

			progress.Show();
			UserNode tmpUser = Utils.UserCredentials.GetUserLogin(this);
			var response = await _loginService.GetResponseLoginUserAsync(
				tmpUser.Email, tmpUser.Password);
         
			if (response != null)
			{
				UserNode user = new UserNode();
				user.Email = tmpUser.Email;
				user.Password = tmpUser.Password;
				user.ID = response.userId;
				user.Token = response.token;
				Constants.userNode = user;

				UserCredentials.Remove(this);
				UserCredentials.SaveUserLogin(user, this);
				IniciarSesion();
			}
			progress.Dismiss();
		}

		private void IniciarComponentes()
		{
			_buttonLogin = FindViewById<Button>(Resource.Id.btn_login);
			_mail = FindViewById<TextView>(Resource.Id.txt_email);
			_password = FindViewById<TextView>(Resource.Id.txt_password);
			_linkRegister = FindViewById<TextView>(Resource.Id.txt_link2);
			_rememberPass = FindViewById<TextView>(Resource.Id.pregunta_1);
		}

		private void IniciarBotones()
		{
			// abrimos la activity para el registro de nuevo usuario
			_linkRegister.Click += delegate
			{
				Intent intent = new Intent(this, typeof(RegistroPorTwitterActivity));
				StartActivity(intent);
				OverridePendingTransition(Resource.Animation.slide_right, Resource.Animation.fade_out);

			};

			//abrimos la activity para recuperar la password
			_rememberPass.Click += delegate
			{

				Intent intent = new Intent(this, typeof(RecuperarPasswordActivity));
				StartActivity(intent);
				OverridePendingTransition(Resource.Animation.slide_right, Resource.Animation.fade_out);


			};

			//Done en teclado
			_password.EditorAction += async delegate (Object sender, TextView.EditorActionEventArgs e)
			{

				if (e.ActionId.Equals(global::Android.Views.InputMethods.ImeAction.Done))
				{
					await IniciarSesionNueva();
				}
			};

			//abrimos la activity Principal
			_buttonLogin.Click += async delegate
			{
				await IniciarSesionNueva();
			};
		}

		private async System.Threading.Tasks.Task IniciarSesionNueva()
		{
			if (ComprobarCampos())
			{

				var response = await _loginService.GetResponseLoginUserAsync(_mail.Text, _password.Text);
				if (response != null)
				{
					GenerarCredencialesNode(response);
					IniciarSesion();
				}
				else
				{
					Toast.MakeText(this, "Logeado no correctamente"
					 , ToastLength.Long).Show();
				}
			}
			else
			{
				Toast.MakeText(this, "Los campos se deben rellenar"
					, ToastLength.Long).Show();
			}
		}

		private void IniciarSesion()
		{

			Toast.MakeText(this, "Logeado correctamente"
		 , ToastLength.Long).Show();
			Intent intent = new Intent(this, typeof(SplashActivity));
			StartActivity(intent);
			OverridePendingTransition(Resource.Animation.fade_in, Resource.Animation.fade_out);

		}

		private void GenerarCredencialesNode(LoginResponse response)
		{
			//Guardando credenciales para los servicios node
			UserNode user = new UserNode();
			user.Email = _mail.Text;
			user.Password = _password.Text;
			user.ID = response.userId;
			user.Token = response.token;
			Constants.userNode = user;
			UserCredentials.Remove(this);
			UserCredentials.SaveUserLogin(user, this);

		}

		public bool ComprobarCampos()
		{
			return _password.Text.Length != 0 && _mail.Text.Length != 0;
		}
	}
}
