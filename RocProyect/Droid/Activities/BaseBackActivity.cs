﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;


namespace RocProyect.Droid.Activities
{
	/// <summary>
	/// Base back activity.Clase base para las activitidades que tiene que ir 
	/// para atras
	/// </summary>
	[Activity(Label = "BaseBackActivity", ScreenOrientation = ScreenOrientation.Portrait)]
	public class BaseBackActivity : AppCompatActivity
	{
		Toolbar toolbar;
		public void initToolBar()
		{
			toolbar = (Toolbar)FindViewById(Resource.Id.toolbar);
			SetSupportActionBar(toolbar);
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetDisplayShowHomeEnabled(true);
			SupportActionBar.SetDisplayShowTitleEnabled(false);

			Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

			if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
				SupportActionBar.SetHomeAsUpIndicator(
					Resources.GetDrawable(Resource.Mipmap.backIcon, null));
			else
				SupportActionBar.SetHomeAsUpIndicator(
					Resources.GetDrawable(Resource.Mipmap.backIcon));
		}

		/// <summary>
		/// Ons the options item selected.
		/// </summary>
		/// <returns>Devolvemos a la pantalla de atras</returns>
		/// <param name="item">Item.</param>
		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				case Android.Resource.Id.Home:
					OnBackPressed();
					OverridePendingTransition(Resource.Animation.slide_left, Resource.Animation.fade_out);

					return true;
			}

			return base.OnOptionsItemSelected(item);
		}
	}
}
