﻿
using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Fragments;
using RocProyect.Droid.Models;

//para las notificaciones
using Com.OneSignal;
using Com.OneSignal.Abstractions;
using System.Timers;
using RocProyect.Services;
using RocProyect.Droid.Utils;

namespace RocProyect.Droid.Activities
{
	/// <summary>
	/// Actividad principal que sigue la estructura de una aplicacion NavigationDrawer
	/// por lo que es la actividad que conecta al resto
	/// </summary>
	[Activity(Label = "RocActivity", Theme = "@style/AppTheme")]
	public class RocActivity : BaseMenuActivity, NavigationView.IOnNavigationItemSelectedListener,
	View.IOnClickListener
	{

		private TextView _userName;
		private ImageView _imageUser;
		private TextView _changeUser;
		private BaseFragment fragment;
		private ProgressDialog _pr;
		private Timer timer;

		private Twitter _twitter;
		private IPushService _pushService;


		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
			try
			{
				SetContentView(Resource.Layout.RocActivity);
				initToolBar();
				OneSignal.Current.StartInit(Constants.KEY_ONESIGNAL)
						 .InFocusDisplaying(OSInFocusDisplayOption.Notification).EndInit();
				OneSignal.Current.IdsAvailable(IdsAvailable);

				InitNavigationDrawer();
				//Poniendo fragment principal
				fragment = new TimeLineFragment();
				this.SupportFragmentManager.BeginTransaction().
				Replace(Resource.Id.content_main, fragment)
									 .Commit();

			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.Message);
			}
		}

		/// <summary>
		/// Enviamos el playerId al servidor para poder enviar notificaciones
		/// al cliente para que pueda recibir notificaciones al usuario
		/// </summary>
		/// <param name="userID">User identifier.</param>
		/// <param name="pushToken">Push token.</param>
		private async void IdsAvailable(string userID, string pushToken)
		{
			Constants.PLAYER_ID = userID;

			_pushService = new PushService();
			var value = await _pushService.InitPush(Constants.userNode.Token);

			if (value)
				Console.WriteLine("usuario Conectado a OneSignal");

		}


		private void InitNavigationDrawer()
		{

			DrawerLayout drawer = (DrawerLayout)FindViewById(Resource.Id.drawer_layout);

			ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, Resource.String.navigation_drawer_open,
				Resource.String.navigation_drawer_close);
			drawer.AddDrawerListener(toggle);
			toggle.SyncState();

			//Iniciando el navigationDrawer
			NavigationView navigationView =
				(NavigationView)FindViewById(Resource.Id.nav_view);
			navigationView.SetNavigationItemSelectedListener(this);
			navigationView.ItemIconTintList = null;
			_twitter = Twitter.getInstance;
			InitHeader(navigationView);

		}

		private void InitHeader(NavigationView navigationView)
		{
			string nombre_completo = "";
			View Hview = navigationView.GetHeaderView(0);

			_imageUser = (ImageView)Hview.FindViewById(Resource.Id.img_profile);

			Glide.With(this).Load(_twitter.Datos_user.imagen)
				 .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform())
				 .Into(_imageUser);


			_userName = (TextView)Hview.FindViewById(Resource.Id.txt_nameProfile);
			_changeUser = (TextView)Hview.FindViewById(Resource.Id.txt_changeProfile);

			_userName.Text = _twitter.Datos_user.nombre;

			_changeUser.SetOnClickListener(this);
			navigationView.SetNavigationItemSelectedListener(this);
		}

		/// <summary>
		/// Metodo que se encarga de gestionar el menu lateral 
		/// </summary>      
		public bool OnNavigationItemSelected(IMenuItem menuItem)
		{
			Boolean fragmentTransaction = false;
			KeyboardUtils.HideKeyboard(this);

			switch (menuItem.ItemId)
			{
				//iconos
				case Resource.Id.nav_timeLine: // VER TIMELINE
					fragment = new TimeLineFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.nav_crearTweet:
					fragment = new CrearTweetFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.nav_misFollowers:
					fragment = new FollowersFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.nav_programadosTweet:
					fragment = new TweetProgramadosFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.nav_notificaciones:
					fragment = new NotificacionesFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.nav_tendencias:  //tendencias
					fragment = new TendenciasFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.nav_menciones:
					fragment = new Fragments.Menciones();
					fragmentTransaction = true;
					break;

				case Resource.Id.nav_exit: //salir session
					CerrarSesion();
					break;
			}

			//Iniciamos el cambio de fragment
			if (fragmentTransaction)
			{
				this.SupportFragmentManager.BeginTransaction().
					Replace(Resource.Id.content_main, fragment)
									  .Commit();
				menuItem.SetChecked(true);
			}
			DrawerLayout drawer = (DrawerLayout)FindViewById(Resource.Id.drawer_layout);
			drawer.CloseDrawer(GravityCompat.Start);
			return true;
		}

		/// <summary>
		/// Aparece un alertDialg donde aparece las opciones para cerrar
		/// sesion
		/// </summary>
		private void CerrarSesion()
		{
			Android.App.AlertDialog.Builder dialog = new Android.App.AlertDialog.Builder(this);
			Android.App.AlertDialog alert = dialog.Create();
			alert.SetTitle("Cerrar sesión");
			alert.SetMessage("¿Estas seguro de querer cerrar sesión?");
			alert.SetIcon(Resource.Mipmap.ayuda50);

			alert.SetButton("Si", (c, ev) =>
			{
				Utils.UserCredentials.Remove(this);
				Intent intent = new Intent(this, typeof(LoginActivity));
				StartActivity(intent);

			});

			alert.SetButton2("No", (c, ev) => { });
			alert.Show();
		}

		public void OnClick(View v)
		{
			switch (v.Id)
			{
				case Resource.Id.txt_changeProfile:
					this.SupportFragmentManager.BeginTransaction().
					Replace(Resource.Id.content_main,
							new EditarPerfil_Fragment())
									  .Commit();
					DrawerLayout drawer = (DrawerLayout)FindViewById(Resource.Id.drawer_layout);
					drawer.CloseDrawer(GravityCompat.Start);
					break;
			}
		}


		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			MenuInflater.Inflate(Resource.Menu.main, menu);
			return true;
		}


		/// <summary>
		/// Se encarga de gestionar el menu que se encuentra 
		/// en la toobar
		/// </summary>
		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			Boolean fragmentTransaction = false;
			BaseFragment fragmentMenu = null;
			switch (item.ItemId)
			{
				case Resource.Id.ic_notificacion:
					fragmentMenu = new NotificacionesFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.ic_pencil:
					fragmentMenu = new CrearTweetFragment();
					fragmentTransaction = true;
					break;

				case Resource.Id.ic_search:
					Intent intent = new Intent(this, typeof(BusquedaActivity));
					StartActivity(intent);
					OverridePendingTransition(Resource.Animation.slide_right, Resource.Animation.fade_out);
					break;
			}
			KeyboardUtils.HideKeyboard(this);
			if (fragmentTransaction)
			{
				this.SupportFragmentManager.BeginTransaction().
					Replace(Resource.Id.content_main, fragmentMenu)
									  .Commit();

			}

			return base.OnOptionsItemSelected(item);
		}

		protected override void OnActivityResult(int requestCode,
												  Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			if (resultCode == Result.Ok)
			{
				Bundle extra = data.Extras;
				String fragmentOrigen;

				try
				{
					fragmentOrigen = extra.GetString("fragment").Trim() ?? "Error";
				}
				catch
				{
					fragmentOrigen = "Error";
				}

				if (fragmentOrigen.Equals("modificarLista"))
				{
					NotificacionesFragment tmp = (NotificacionesFragment)fragment;
					tmp.IniciarListaAsync();
				}
			}
		}


	}
}
