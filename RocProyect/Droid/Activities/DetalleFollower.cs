﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.View;
using RocProyect.Droid.Fragments;
using Android.Support.Design.Widget;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Droid.Activities
{
	/// <summary>
	/// En esta pantalla aparece los principales detalles del usuario,
	/// esta actividad sirve como contenedor para los fragment detalles follower y
	/// DetallesFollowerTimeLine
	/// </summary>
	[Activity(Label = "DetalleFollower", Theme = "@style/AppTheme")]
    public class DetalleFollower : BaseBackActivity 
    {

		private SectionsPagerAdapterGeneral mSectionsPagerAdapter;
        private ViewPager mViewPager;
        private Follower _follower;
        public Follower Follower { get => _follower; set => _follower = value; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.DetallesFollower);

            //Obtengo el follower del que voy a conseguir los datos
            string detalleJSON = Intent.GetStringExtra("detalleFollower") ?? "Error";
            if (detalleJSON != "Error")
            {
                _follower = JSON.DeserializeFollowerJSON(detalleJSON);
            }

			List<BaseFragment> tmp = new List<BaseFragment>{new DetallesFollower_Datos(),
				new DetallesFollower_TimeLine()};

            mSectionsPagerAdapter = new SectionsPagerAdapterGeneral
                (this.SupportFragmentManager,
                 tmp);

            mViewPager = (ViewPager)FindViewById(Resource.Id.container);
            mViewPager.Adapter=mSectionsPagerAdapter;
            initToolBar();

            TabLayout tabLayout = (TabLayout)FindViewById(Resource.Id.tabs);

            mViewPager.AddOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.AddOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        }
    }

   
 
}
