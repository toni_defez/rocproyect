﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using RocProyect.Services;
using Tweetinvi;
using Tweetinvi.Models;

namespace RocProyect.Droid.Activities
{
	/// <summary>
    /// Pantalla para el registro de usuario, el usuario tendra que rellenar una serie de
	/// campos y despues tendra que obtener el ping. Dicho ping lo conseguira a traves
	/// de darle el permiso  a nuestra cuenta de twitter para que use la aplicacion
    /// </summary>
	[Activity(Label = "RegistroPorTwitterActivity", Theme = "@style/AppTheme")]
	public class RegistroPorTwitterActivity : BaseBackActivity
	{

		private TwitterCredentials appCredentials;
		private IAuthenticationContext authenticationContext;

		private EditText _txtEmail;
		private EditText _txtPass;
		private EditText _txtPass2;
		private EditText _txtPing;
		private TextView _lblPing;
		private Button _btnlogin;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
			SetContentView(Resource.Layout.RegistroPorTwitter);
			base.initToolBar();

			_txtEmail = (EditText)FindViewById(Resource.Id.txt_nombre);
			_txtPass = (EditText)FindViewById(Resource.Id.txt_apellidos);
			_txtPass2 = (EditText)FindViewById(Resource.Id.txt_email);
			_txtPing = (EditText)FindViewById(Resource.Id.txt_telefono);
			_lblPing = (TextView)FindViewById(Resource.Id.txt_link2);
			_btnlogin = (Button)FindViewById(Resource.Id.btn_login);
			IniciarBotones();
		}

        /// <summary>
        /// Le da el funcionamiento a los botones de la aplicacion
        /// </summary>
		private void IniciarBotones()
		{
			_lblPing.Click += delegate
			{
				IniciarNavegador();
			};

			_txtPing.EditorAction += delegate (Object sender, TextView.EditorActionEventArgs e)
			{

				if (e.ActionId.Equals(global::Android.Views.InputMethods.ImeAction.Done))
				{
					LoginPorPingAsync();
				}
			};

			_btnlogin.Click += delegate
			{
				LoginPorPingAsync();
			};
		}

        /// <summary>
		/// Abrimos el navegador para que el usuario pueda abrir su cuenta y obtener
		/// el ping para darnos acceso a su cuenta
        /// </summary>
		private void IniciarNavegador()
		{
			appCredentials = new TwitterCredentials(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
			authenticationContext = AuthFlow.InitAuthentication(appCredentials);
			var uri = Android.Net.Uri.Parse(authenticationContext.AuthorizationURL);
			var intent = new Intent(Intent.ActionView, uri);
			StartActivity(intent);         
		}
              
        /// <summary>
        /// Una vez obtenindo el token ya podemos logearnos
        /// </summary>
		public async void LoginPorPingAsync()
		{         
			if(authenticationContext == null)
				Toast.MakeText(this, "Debemos solicitar el ping primero", ToastLength.Long).Show();
			else if (_txtPing.Text.Length==0)
				Toast.MakeText(this, "El ping no puede ser vacio", ToastLength.Long).Show();
			else
			{
				var pinCode = _txtPing.Text;

				// Con este codigo podemos conseguir las credenciales necesarias para
				// el uso de twitter desde nuestra aplicacion
				var userCredentials = AuthFlow.CreateCredentialsFromVerifierCode(pinCode, authenticationContext);
				if (userCredentials == null)
					Toast.MakeText(this, "Ping Incorrecto", ToastLength.Long).Show();
				else
					await RegistrarUsuario(userCredentials);               
			}
         
		}
        /// <summary>
        /// Registramos al usuario en el servidor con las claves correctas
        /// </summary>
        /// <returns>The usuario.</returns>
        /// <param name="userCredentials">User credentials.</param>
		private async System.Threading.Tasks.Task RegistrarUsuario(ITwitterCredentials userCredentials)
		{
			//Guardamos las credenciales
			Constants.ACCES_TOKEN = userCredentials.AccessToken;
			Constants.ACCES_SECRET_TOKE = userCredentials.AccessTokenSecret;

			// Usamos la credenciales en nuestra aplicacion
			Auth.SetCredentials(userCredentials);

			IAuthenticatedUser user = User.GetAuthenticatedUser();

			if (ComprobarCampos())
			{
				string password1 = _txtPass.Text;
				string email1 = _txtEmail.Text;
				IUserService _registrarUsuario = new UserService();
				bool resp = await _registrarUsuario.GetResponseRegisterUser(
				 email1, password1);

				if (resp)
					Toast.MakeText(this, "Usuario dado de alta"
					, ToastLength.Long).Show();

				else
					Toast.MakeText(this, "El usuario no dado de alta"
					, ToastLength.Long).Show();
			}
		}

		private Boolean IsValidEmail(String email)
		{
			return Android.Util.Patterns.EmailAddress.Matcher(email).Matches();
		}
        
        /// <summary>
        /// Comprobamos todos los campos para asegurarnos de que el regitro 
		/// cumple los requisitos
        /// </summary>
        /// <returns><c>true</c>, if campos was comprobared, <c>false</c> otherwise.</returns>
		private bool ComprobarCampos()
		{
			if (_txtEmail.Length() == 0 || _txtPass.Length() == 0
		    || _txtPass2.Length() == 0|| _txtPing.Length() == 0)
			{
				Toast.MakeText(this, "No puedes dejar ningun campo vacio"
						   , ToastLength.Long).Show();
				return false;
			}

			else if (!IsValidEmail(_txtEmail.Text))
			{
				Toast.MakeText(this, "El formato del correo no es correcto"
						   , ToastLength.Long).Show();
				return false;
			}

			else if (_txtPass.Length() < 8)
			{
				Toast.MakeText(this, "La contraseña es de 8 caracteres minimo"
						   , ToastLength.Long).Show();
				return false;
			}

			else if (!_txtPass.Text.Equals(_txtPass2.Text))
			{
				Toast.MakeText(this, "Las contraseñas no coinciden"
						   , ToastLength.Long).Show();
				return false;
			}
			return true;
		}
      
	}
}
