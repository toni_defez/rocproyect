﻿

using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Widget;
using RocProyect.Droid.Fragments;
using RocProyect.Droid.Models;
using RocProyect.Models;

namespace RocProyect.Droid.Activities
{
	/// <summary>
    /// Pantalla que tiene los campos de busqueda. Es una actividad que alberga 
	/// un ViewPager donde encontramos dos fragments diferentes BusquedaTweetFragment
	/// y BusquedaUsuarios 
    /// </summary>
	[Activity(Label = "BusquedaActivity", Theme = "@style/AppTheme")]
	public class BusquedaActivity : BaseBackActivity
	{
		private ImageView _imgSend;
		private EditText _textBusqueda;
		private SectionsPagerAdapterGeneral mSectionsPagerAdapter;
		private ViewPager _mViewPager;

		private List<MiTweet> _listTweets;
		private List<Follower> _listFollowers;

		private BusquedaTweetsFragment _busquedaTweets;
		private BusquedaUsuariosFragment _busquedaUsuarios;
		private ProgressDialog _pr;

		private Twitter _twitter;



		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.BusquedaFragment);

			base.initToolBar();
			_busquedaTweets = new BusquedaTweetsFragment();
			_busquedaUsuarios = new BusquedaUsuariosFragment();

			_twitter = Twitter.getInstance;
			_imgSend = (ImageView)FindViewById(Resource.Id.imgSend);
			_textBusqueda = (EditText)FindViewById(Resource.Id.textBusqueda);

			List<BaseFragment> tmp = new List<BaseFragment>{_busquedaTweets, _busquedaUsuarios
				};

			mSectionsPagerAdapter = new SectionsPagerAdapterGeneral
				(this.SupportFragmentManager,
				 tmp);

			_mViewPager = (ViewPager)FindViewById(Resource.Id.viewpager);
			_mViewPager.Adapter = mSectionsPagerAdapter;

			TabLayout tabLayout = (TabLayout)FindViewById(Resource.Id.tabs);

			_mViewPager.AddOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
			tabLayout.AddOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(_mViewPager));


			_imgSend.Click += async delegate
			{

				if (_textBusqueda.Length() != 0)
				{
					Utils.KeyboardUtils.HideKeyboard(this);
					_pr = new ProgressDialog(this);
					_pr.SetMessage("Buscando...");
					_pr.Show();

					string texto = _textBusqueda.Text;
					_listFollowers = await _twitter.ObtenerFollowersPorTexto(texto);
					_listTweets = await _twitter.BuscarTweetsPorTextoAsync(texto);

					_pr.Dismiss();

					_busquedaTweets.TimeLineList = _listTweets;
					_busquedaUsuarios.TimeLineList = _listFollowers;

					_busquedaTweets.ActualizarListaAsync();
					_busquedaUsuarios.ActualizarListaAsync();

				}

			};
		}
	}
}
