﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Models;
using RocProyect.Models;

using System.Threading;
using Android.Util;
using RocProyect.Services;
using Android.Content.PM;

namespace RocProyect.Droid.Activities
{

    /// <summary>
    /// Pantalla que se encarga de la pantalla splash donde se iniciar 
	/// la instancia de twitter y se autenticar el cliente con los resultados
    /// </summary>
	[Activity(Theme = "@style/AppTheme.Splash",
	          ScreenOrientation = ScreenOrientation.Portrait,
			  NoHistory = true)]
	public class SplashActivity : AppCompatActivity
	{

		private DetallesUsuarioResponse _detallesUsuario;
		private Twitter _twitter;
		private ProgressDialog _pr;
		private IUserService _loginService;


		public override View OnCreateView(string name, Context context, IAttributeSet attrs)
		{
			return base.OnCreateView(name, context, attrs);
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Splash);
			_loginService = new UserService();

			RunOnUiThread(async () =>
			{
				_pr = new ProgressDialog(this);
				_pr.Show();
				var response2 = await _loginService.GetResponseDetallesUsuarioAsync(
					Constants.userNode.Token, Constants.userNode.ID);
				DetallesUsuarioResponse details = Constants.KeyUsuario;
				if (Twitter.getInstance == null)
					Twitter.Create(details);

				_pr.Dismiss();


				InitLoginAsync();

			});


		}

		static void IniciarTwitter()
		{
			DetallesUsuarioResponse details = Constants.KeyUsuario;

			if (Twitter.getInstance == null)
				Twitter.Create(details);         
		}

		private async void InitLoginAsync()
		{
			Twitter.getInstance.IniciarDatos();
			Constants.LIST_FOLLOWERS = await Twitter.getInstance.ObtenerFollowersAsync();
			Constants.LIST_TWEET_TIMELINE = await Twitter.getInstance.ObtenerTimeLineAsync();
			Constants.LIST_CHATS = await Twitter.getInstance.ObtenerMensajeAsync();
			Intent intent = new Intent();
			intent = new Intent(Android.App.Application.Context,
								typeof(RocActivity));
			base.StartActivity(intent);
		}
	}
}
