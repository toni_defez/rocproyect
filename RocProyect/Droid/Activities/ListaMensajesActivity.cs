﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Droid.Utils;
using RocProyect.Models;
using RocProyect.Utils;
using Tweetinvi;

namespace RocProyect.Droid.Activities
{
	[Activity(Label = "ListaMensajesActivity",Theme = "@style/AppTheme")]
	public class ListaMensajesActivity : BaseBackActivity
    {
		private RecyclerView mMessageRecycler;
        private MensajeAdapter mMessageAdapter;
		private Chat _chat;
		private List<Mensaje> misMensajes;
		private EditText _texto;
		private Button _sendButton;
		private Twitter _twitter;
		LinearLayoutManager mLinearLayoutManager;
		ScrollView _scrollView;
		      
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
			Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
			SetContentView(Resource.Layout.activity_message_list);
            base.initToolBar();

            
			var stream = Stream.CreateUserStream();
            stream.MessageReceived += (sender, args) => {
				if (args.Message.SenderId == _chat.IdUsuario)
				{
					Mensaje tmp = Twitter.getInstance.ConvertirMensaje(args.Message);
					misMensajes.Add(tmp);
					RunOnUiThread(() => {
						ActualizarMensajes();
					});

				}
            };
			stream.StartStreamAsync();

            // Create your application here
			//detalleChat
			string chatJSON = Intent.GetStringExtra("detalleChat") ?? "Error";
			if (chatJSON != "Error")
            {
				_chat = JSON.DeserializeChatJSON(chatJSON);
				misMensajes = _chat.Conversacion;
            }


			if (_chat != null)
			{
				mMessageRecycler = (RecyclerView)FindViewById(Resource.Id.reyclerview_message_list);

				ActualizarMensajes();
				_scrollView = (ScrollView)FindViewById(Resource.Id.scrollView1);
				_sendButton = (Button)FindViewById(Resource.Id.button_chatbox_send);
				_texto = (EditText)FindViewById(Resource.Id.edittext_chatbox);
				_twitter = Twitter.getInstance;
				ScrollAbajo();

	            

				_sendButton.Click += async delegate
				{

					if (_texto.Length() != 0)
					{
						string texto = _texto.Text;
						bool result = await _twitter.EnviarMensajeAsync(texto, _chat.IdUsuario);

						if (result == true)
						{
							Mensaje tmp = new Mensaje();
							tmp.IdSender1 = _twitter.user.Id;
							tmp.Texto = texto;
							misMensajes.Add(tmp);
							_scrollView.PageScroll(FocusSearchDirection.Down);
							tmp.Texto = "";
							KeyboardUtils.HideKeyboard(this);
							ActualizarMensajes();
						}

					}

				};
			}


		}

		private void ScrollAbajo()
		{
			_scrollView.Post(async () =>
			{
				await Task.Run(() =>
				{
					return _scrollView.FullScroll(FocusSearchDirection.Down);
				});


			});
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
					
                    OnBackPressed();

					Intent i = new Intent();
                    Bundle extra = new Bundle();
                    extra.PutString("fragment", "modificarLista");
                    i.PutExtras(extra);
                    SetResult(Android.App.Result.Ok, i);
                    Finish();
					break;
            }

            return base.OnOptionsItemSelected(item);
        }

              
		private void ActualizarMensajes()
		{
			mMessageAdapter = new MensajeAdapter(misMensajes, this);
			mLinearLayoutManager = new LinearLayoutManager(this);

			mMessageRecycler.SetLayoutManager(mLinearLayoutManager);

			mMessageRecycler.SetAdapter(mMessageAdapter);


			if (_scrollView != null)
				ScrollAbajo();


		}



	}
}
