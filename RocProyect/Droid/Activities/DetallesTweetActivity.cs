﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using RocProyect.Droid.Fragments;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Droid.Activities
{
	/// <summary>
    /// Detalles tweet  donde prodremos informacion relevante sobre el tweet
	/// esta pantalla alberga dos fragments DetallesTweet_datos y
	///  DetallewTweet_TimeLine
    /// </summary>
	[Activity(Label = "DetallesTweetActivity", Theme = "@style/AppTheme")]
	public class DetallesTweetActivity : BaseBackActivity
	{
		private SectionsPagerAdapterGeneral mSectionsPagerAdapter;
		private ViewPager mViewPager;
		private MiTweet _tweet;
		public MiTweet Tweet { get => _tweet; set => _tweet = value; }
		public List<MiTweet> ListTweetsReply { get => _listTweetsReply; set => _listTweetsReply = value; }
		public List<Follower> ListUsuariosReply { get => _listUsuariosReply; set => _listUsuariosReply = value; }

		private List<MiTweet> _listTweetsReply;
		private List<Follower> _listUsuariosReply;
		private Twitter _twitter;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.DetallesFollower);
			base.initToolBar();
			_twitter = Twitter.getInstance;

			//Obtengo el follower del que voy a conseguir los datos
			string direccionJSON = Intent.GetStringExtra("detallesTweet") ?? "Error";
            if (direccionJSON != "Error")
            {
				_tweet = JSON.DeserializeMiTweetJSON(direccionJSON);
            }
		
            
			List<BaseFragment> tmp = new List<BaseFragment>{new DetallesTweet_Datos(), 
				new DetallesTweet_TimeLine()};

            mSectionsPagerAdapter = new SectionsPagerAdapterGeneral
                (this.SupportFragmentManager,
                 tmp);
                          
			mViewPager = (ViewPager)FindViewById(Resource.Id.container);
			mViewPager.Adapter = mSectionsPagerAdapter;

			TabLayout tabLayout = (TabLayout)FindViewById(Resource.Id.tabs);

			mViewPager.AddOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
			tabLayout.AddOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
            
		}
       
	}




}
