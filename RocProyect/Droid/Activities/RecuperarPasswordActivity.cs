﻿
using System;
using Android.App;
using Android.OS;
using Android.Widget;
using RocProyect.Services;

namespace RocProyect.Droid.Activities
{
	/// <summary>
    /// Pantalla encargada de enviar el mensaje para recuperar el 
	/// password
    /// </summary>
	[Activity(Label = "RecuperarPasswordActivity", Theme = "@style/AppTheme")]
	public class RecuperarPasswordActivity : BaseBackActivity
	{
		private EditText _email;
		private Button _btnEnviar;
		private UserService _userservice;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			_userservice = new UserService();
			SetContentView(Resource.Layout.RecuperarPassword);
			base.initToolBar();

			_email = (EditText)FindViewById(Resource.Id.txt_email);
			_btnEnviar = (Button)FindViewById(Resource.Id.btn_login);

			_btnEnviar.Click += async delegate
			{            
				string email = _email.Text;
				if (IsValidEmail(email))
				{
					bool resultado = await _userservice.GetRecuperarContra(email);
					if (resultado)
					{
						AlertDialog.Builder dialog = new AlertDialog.Builder(this);
						AlertDialog alert = dialog.Create();
						alert.SetTitle("Atención");
						alert.SetMessage("Se ha enviado un correo a la cuenta " + email + ". Por favor reviselo");
						alert.SetIcon(Resource.Mipmap.ayuda50);

						alert.SetButton("Ok", (c, ev) =>
						{ });
						alert.Show();
					}
					else
					{
						Toast.MakeText(this, "Usuario no encontrado", ToastLength.Long).Show();
					}
				}
				else
				{
					Toast.MakeText(this, "Introduzca un email valido", ToastLength.Long).Show();
				}

			};
		}

		private Boolean IsValidEmail(String email)
		{
			return Android.Util.Patterns.EmailAddress.Matcher(email).Matches();
		}

	}
}
