﻿using Android.App;
using Android.Content.PM;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;


namespace RocProyect.Droid.Activities
{
	/// <summary>
	/// Menu base para todas las actividades se encarga de definir el comportamiento
	/// de la toolbar
	/// </summary>
	[Activity(Label = "BaseMenuActivity", ScreenOrientation = ScreenOrientation.Portrait)]
	public class BaseMenuActivity : AppCompatActivity

	{
		protected Toolbar toolbar;
		public Android.Widget.ImageButton _btnOpenDrawer;
		DrawerLayout drawerLayout;
		NavigationView navigationView;

        /// <summary>
        /// Iniciamos la toolbar para las actividades
        /// </summary>
		public void initToolBar()
		{
			toolbar = (Toolbar)FindViewById(Resource.Id.toolbar);
			SetSupportActionBar(toolbar);
			Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
			SupportActionBar.SetLogo(Resource.Mipmap.logo);
			SupportActionBar.SetDisplayUseLogoEnabled(true);
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetDisplayShowTitleEnabled(false);
			SupportActionBar.SetHomeButtonEnabled(true);
			SupportActionBar.SetHomeAsUpIndicator(Resource.Mipmap.menu_three_lines);

			drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				case Android.Resource.Id.Home:
					drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
					return true;
			}
			return base.OnOptionsItemSelected(item);
		}



	}
}
