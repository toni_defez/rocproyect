﻿using System;
using System.Collections.Generic;
using RocProyect.Models;
using Tweetinvi;
using Tweetinvi.Core.Extensions;
using Tweetinvi.Models;
using Tweetinvi.Parameters;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Linq;

namespace RocProyect.Droid.Models
{
    public class Twitter
    {
      
        private string CONSUMER_KEY;//= "FIagYsQNtkbYWmvHcxTB2owUy";
        private string CONSUMER_SECRET;// = "dPRlKf20pTG1TkhNDuL1w6MlGB1yBH97atzAA0TitTK4AOnHBa";
        private string ACCESS_TOKEN;// = "1291967058-iMNUmrD76zjQFA771ydsm2dtkYM77yascyTUAQB";
        private string ACCESS_TOKEN_SECRET ;// = "OXQQkw2jtDlCqGqKcbL6v5xWFaYzMO0qwTpRrTG3rmppq";
        public Tweetinvi.Models.IAuthenticatedUser user;

        private DatosUsuario datos_user;

        public DatosUsuario Datos_user { get => datos_user; set => datos_user = value; }
        public List<MiTweet> ListTweet { get => _listTweet; set => _listTweet = value; }
        public List<Follower> ListFollowers { get => _listFollowers; set => _listFollowers = value; }

        private List<MiTweet> _listTweet;
        private List<Follower> _listFollowers;
		private List<Tendencia> _listTendencias;
		public List<Tendencia> ListTendencias { get => _listTendencias; set => _listTendencias = value; }

        private static Twitter instance;



        public static Twitter getInstance
        {
            get
            {
                return instance;
            }
        }

	

		public static void Create(DetallesUsuarioResponse newUser)
        {
            if (instance != null)
            {
             throw new Exception("Object already created");
            }
            instance = new Twitter(newUser);             
        } 



        public Twitter(DetallesUsuarioResponse newUser)
        {
            CONSUMER_KEY = newUser.consumerKey;
            CONSUMER_SECRET = newUser.consumerSecret;
            ACCESS_TOKEN = newUser.accesToken;
            ACCESS_TOKEN_SECRET = newUser.accesTokenSecret;         

        }
        
        public async void IniciarDatos()
		{
			Datos_user = await Task.Run(() => { return ObtenerDatosAsync(); });
		}

		private  DatosUsuario ObtenerDatosAsync()
        {
			Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
            ACCESS_TOKEN_SECRET);
            user = Tweetinvi.User.GetAuthenticatedUser();

			string imagen = user.ProfileImageUrl400x400;
            DateTime fecha = user.CreatedAt;
            string descripcion = user.Description;
            int friendsCount = user.FriendsCount;
            int follersCount = user.FollowersCount;
            String lengua = user.Language.ToString();
            String location = user.Location;
			DatosUsuario user1 = new DatosUsuario(imagen, fecha, friendsCount, follersCount,
                descripcion, lengua, location);
			user1.nombre = user.Name;
			return user1;
            
        }
        
		public void ObtenerTrendingPorCoordenadas(Coordenadas current)
		{
			var closestTrendLocations = Trends.GetClosestTrendLocations(current.x, current.y);
		}


		public async Task<List<Tendencia>> ObtenerTrendingTopicAsyncSpainAsync()
		{
            
			var trends = await TrendsAsync.GetTrendsAt(Constants.idEspaña);

			var trendTermToSearch = trends.Trends.ToArray();
			ListTendencias = new List<Tendencia>();
            
			foreach(ITrend value in trendTermToSearch)
			{
				Tendencia tmp = new Tendencia();
				tmp.Nombre= value.Name;
				tmp.Volumen = value.TweetVolume;
				//value.TweetVolume
				ListTendencias.Add(tmp);
			}
			return ListTendencias;

		     
		}





		//TODO IMPLEMENTAR UN LISTADO CON MIS ULTIMOS TWEETS
		//PARA HACER LA FUNCIONALIDAD DE PODER BORRARLOS

		//// Get the latest 40 tweets published on the user timeline
		//var tweets = Timeline.GetUserTimeline(< user_identifier >);

		//// Get more control over the request with a UserTimelineParameters
		//var userTimelineParameters = new UserTimelineParameters();
		//var tweets = Timeline.GetUserTimeline(< user_identifier >, userTimelineParameters);


		//public void COSAS(){
		//    var stream = Stream.CreateSampleStream();
		//    stream.TweetReceived += (sender, args) =>
		//    {
		//        // Do what you want with the Tweet.
		//        Console.WriteLine(args.Tweet);
		//    };
		//    stream.StartStream();

		//}

        #region Tweets
		private  List <MiTweet> ConvertirMiTweet(IEnumerable<ITweet> timelineTweets)
		{
			List<MiTweet> tweets = new List<MiTweet>();         
            foreach (var tweet in timelineTweets)
            {
                MiTweet tmp = new MiTweet();
                tmp.id = tweet.Id;
                tmp.text = tweet.Text;
                tmp.time = tweet.CreatedAt.ToShortDateString();
                tmp.retweets = tweet.RetweetCount;
                tmp.favourites = tweet.FavoriteCount;
                int quote = 0; //(int)timelineTweet.QuotedStatusId;

                Tweetinvi.Models.IUser user = tweet.CreatedBy;
                tmp.nombre_usuario = user.Name;
                tmp.imagen_usuario = user.ProfileImageUrl400x400;
				tmp.usuario = user.Id;
                tmp.favorito = tweet.Favorited;
                tmp.retweet = tweet.Retweeted;

                if (tweet.Media.Count != 0)
                {
                    List<string> listTweet = new List<string>();
                    for (int i = 0; i < tweet.Media.Count; i++)
                    {
                        listTweet.Add(tweet.Media[i].MediaURLHttps);
                    }
                    tmp.list_imagenes = listTweet;
                    tmp.imagen_tweet = listTweet[0];
                }
                else
                    tmp.imagen_tweet = null;
                tweets.Add(tmp);

            }
            return tweets;         
		}


        public async Task<List<MiTweet>> ObtenerTimeLineAsync()
        {         
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
                ACCESS_TOKEN_SECRET);
            user = User.GetAuthenticatedUser();
            
			IEnumerable<ITweet> timelineTweets = await TimelineAsync.GetHomeTimeline(20);         
			return ConvertirMiTweet(timelineTweets);
        }

		public async Task<List<MiTweet>> ObtenerTimeLineRespuesta(MiTweet temp)
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
                ACCESS_TOKEN_SECRET);
            user = User.GetAuthenticatedUser();
			ITweet tweetChoice = Tweet.GetTweet(temp.id);

			IEnumerable<ITweet> timelineTweets = await SearchAsync.SearchRepliesTo(tweetChoice,false);
            return ConvertirMiTweet(timelineTweets);
        }

		public async Task<List<MiTweet>> BuscarTweetsPorTextoAsync(string tema)
        {
			Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
                ACCESS_TOKEN_SECRET);
            user = User.GetAuthenticatedUser();
            
			var searchParameter = new SearchTweetsParameters(tema)
            {
				Lang = LanguageFilter.Spanish,
                SearchType = SearchResultType.Popular,
                MaximumNumberOfResults = 40,
				Until = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)
            };
                     
			IEnumerable<ITweet> timelineTweets = await SearchAsync.SearchTweets(searchParameter);
			return ConvertirMiTweet(timelineTweets);
        }

		public async Task<List<MiTweet>> TimeLineFollowerAsync(Follower follower)
        {
            var id = follower.id_follower;
            IEnumerable<ITweet> timelineTweets = await TimelineAsync.GetUserTimeline(id, 20);
            return ConvertirMiTweet(timelineTweets);

        }

        #endregion


		#region Followers
		public List<Follower> ConvertirListaFollower(IEnumerable<IUser> followers)
		{
			List<Follower> follows = new List<Follower>();
			foreach (var follower in followers)
            {
				Follower temp = ConvertirFollower(follower);
                follows.Add(temp);
            }
            return follows;
		}

		public Follower ConvertirFollower(IUser follower)
		{
			string line = follower.Name;
            Follower temp = new Follower();
            temp.id_follower = follower.Id;
            temp.Nombre = follower.Name;
            temp.Imagen = follower.ProfileImageUrl400x400;
            temp.Fecha_Creacion = follower.CreatedAt;
            temp.ContSeguidores = follower.FollowersCount;
            temp.ContFav = follower.FavouritesCount;
			temp.ContFriends = follower.FriendsCount;
            temp.estado = follower.Description;
            temp.following = follower.Following;
			var tm = follower.Retweets;
			if (tm != null)
				temp.retweets = tm.Count;
         
			temp.lengua = follower.Language.ToString();
			temp.pais =follower.Location;

			return temp;
		}      

		public async Task<List<Follower>> ObtenerFollowersPorTexto(string texto)
		{
			Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
            ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
            
			IEnumerable<IUser> users = await Task.Run(() => { return Search.SearchUsers(texto); });
			return ConvertirListaFollower(users);
		}


        public async Task<List<Follower>> ObtenerFollowersAsync()
        {
            
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN,
            ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
			IEnumerable<IUser> followers =  await UserAsync.GetFollowers(Convert.ToInt64(user.Id), 10);
			return ConvertirListaFollower(followers);
        }

        //Obtener los follower de un follower
        public async Task<List<Follower>> ObtenerFollowerAsync(Follower follower)
        {
            var user = User.GetUserFromId(follower.id_follower);
			IEnumerable<IUser> listFollower = await user.GetFollowersAsync();
			return ConvertirListaFollower(listFollower);
        }


        public async Task<List<Follower>> ObtenerFriendsAsync(Follower follower)
        {
            var user = User.GetUserFromId(follower.id_follower);
			IEnumerable<IUser> listFollower = await user.GetFriendsAsync();
			return ConvertirListaFollower(listFollower);
        }      
        #endregion


		public bool EmpezarASeguirUsuario(long id)
        {
            var authenticatedUser = User.GetAuthenticatedUser();
			var userToFollow = User.GetUserFromId(id);

            if (authenticatedUser.FollowUser(userToFollow))
            {
				return true;
            }
			return false;
        }
              
		public void HacerNuevoTweet(string texto)
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET,
                ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            
            var user = User.GetAuthenticatedUser();
            var tweet = Tweet.PublishTweet(texto);
        }


		public void HacerNuevoTweetConLocalizacion(Coordenadas current)
        {

            //14 y 16
			var France = new Coordinates(38.35278588846223,-0.4747708925687547);
            var tweet = Tweet.PublishTweet("Some love from spain!", new PublishTweetOptionalParameters
            {
                Coordinates = France
            });

        }


        //hacer favorito
        public bool HacerFavorito(MiTweet tmp)
        {
            bool success = Tweet.FavoriteTweet(tmp.id);
            return success;
        }

        //hacer retweet
        public void HacerRetweet(MiTweet temp)
        {
            var retweet = Tweet.PublishRetweet(temp.id);
        }

        //contestar tweet
		public async Task<bool> ContestarTweetAsync(MiTweet temp, string texto)
        {
            var tweetToReplyTo = Tweet.GetTweet(temp.id);
            
            // We must add @screenName of the author of the tweet we want to reply to
            var textToPublish = string.Format("@{0} {1}", tweetToReplyTo.CreatedBy.ScreenName, texto);
			var tweet =await TweetAsync.PublishTweetInReplyTo(textToPublish, temp.id);
			return (tweet != null);
        }
        
        

		public async Task<List<Follower>> UsuarioRetweetAsync(MiTweet tweet)
		{
			var result = await Task.Run(() => { return UsuarioRetweet2(tweet); });
			return result;
		}

		public List<Follower> UsuarioRetweet2(MiTweet tweet)
		{
			var tweetId = tweet.id;
			var retweeterIds = Tweet.GetRetweetersIds(tweetId);


			var retweeters = User.GetUsersFromIds(retweeterIds);
			List<Follower> followsRetweet = new List<Follower>();
			foreach (var twtid in retweeters)
			{
				Follower tmp = new Follower();
				tmp.id_follower = twtid.Id;
				tmp.Nombre = twtid.Name;
				tmp.Imagen = twtid.ProfileImageUrl400x400;
				tmp.Fecha_Creacion = twtid.CreatedAt;
				tmp.ContSeguidores = twtid.FollowersCount;
				tmp.ContFav = twtid.FavouritesCount;
				tmp.estado = twtid.Description;
				tmp.following = twtid.Following;
				followsRetweet.Add(tmp);
			}
			return followsRetweet;
		}



		public async Task<List<Chat>> ObtenerMensajeAsync()
		{
			
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET,
                ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
			//sender para los recibidios
            //recipientId para los que envio
			IEnumerable<IMessage> latestMessagesReceived = await MessageAsync.GetLatestMessagesReceived();
			IEnumerable<IMessage> latestMessagesSend = await MessageAsync.GetLatestMessagesSent();
			List<IMessage> totalmessages = latestMessagesSend.Union(latestMessagesReceived).ToList();
			List<long> listId = new List<long>();

			long identificadorUsuario = user.Id;
			foreach (IMessage message in totalmessages)
            {
                var t = message;
                long id1 = message.SenderId;
				long id2 = message.RecipientId;
				if(!listId.Contains(id1) && id1!=identificadorUsuario)
                    listId.Add(id1);
				if (!listId.Contains(id2) && id2 != identificadorUsuario)
                    listId.Add(id2);
            }
			//listId.Add(identificadorUsuario);


			//con esto ya tenemos guardados todos los id de los usuarios con los 
			//que hemos interactuado.

			List<Chat> misChats = new List<Chat>();
			foreach(long id in listId)
			{
				
				Chat temp = new Chat(id);
				temp.Conversacion = ObtenerMensajes( id, latestMessagesReceived,latestMessagesSend);
				var user = User.GetUserFromId(id);
				temp.Fecha = temp.Conversacion.Last().Fecha;
				temp.FotoUsuario = user.ProfileImageUrl400x400;
				temp.NombreUsuario = user.Name;
				temp.UltimoMensaje = temp.Conversacion.Last().Texto;
				misChats.Add(temp);
			}
         
			foreach(var chat in misChats)
			{
				var chatq = chat.Conversacion;
			}

			return misChats;
		}

		private List<Mensaje> ObtenerMensajes(long id, IEnumerable<IMessage> 
		                                      latestMessagesReceived, IEnumerable<IMessage> latestMessagesSend)
		{
			List<Mensaje> listmensajes = new List<Mensaje>();
			List<IMessage> messageRecididos =
                    latestMessagesReceived.Where(item => item.SenderId == id).ToList<IMessage>();
            List<IMessage> messagesEnviados =
                latestMessagesSend.Where(item => item.RecipientId == id).ToList<IMessage>();
			List<IMessage> total = messageRecididos.Union(messagesEnviados).ToList();
            
            listmensajes = total.Select(x => new Mensaje()
            {
                IdSender1 = x.SenderId,
                Fecha = x.CreatedAt,
                Texto = x.Text,
                Id = x.Id,
				Sender = new Usuario(){ Nickname = x.Sender.Name,
					    ProfileUrl = x.Sender.ProfileImageUrl400x400}
            }).OrderBy(x => x.Fecha).ToList<Mensaje>();
            return listmensajes;
		}

		public Mensaje ConvertirMensaje(IMessage mensaje)
		{
			Mensaje tmp = new Mensaje();
			tmp.IdSender1 = mensaje.SenderId;
			tmp.Fecha = mensaje.CreatedAt;
			tmp.Texto = mensaje.Text;
			tmp.Sender = new Usuario()
			{
				Nickname = mensaje.Sender.Name,
				ProfileUrl = mensaje.Sender.ProfileImageUrl400x400
			};
			return tmp;
		}



		public async Task<bool> EnviarMensajeAsync(string texto,long destino)
		{
			try
			{
				var message = await MessageAsync.PublishMessage(texto, destino);
				return message.IsMessagePublished;
			}
			catch(Exception e)
			{
				return false;
			}

		}

		public async Task<List<MiTweet>> ObtenerTweetsFavoritosAsync(Follower follower)
        {
			var follow = await UserAsync.GetUserFromId(follower.id_follower);
			IEnumerable<ITweet> favorites = follow.GetFavorites();

			return ConvertirMiTweet(favorites);
        }
      

		public async Task<List<MiTweet>> ObtenerMencionesAsync()
		{
			Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET,
            ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            var user = User.GetAuthenticatedUser();
            var mentionsTimelineParameters = new MentionsTimelineParameters();
            mentionsTimelineParameters.MaximumNumberOfTweetsToRetrieve = 20;
			var tweets =  await TimelineAsync.GetMentionsTimeline(mentionsTimelineParameters);

			return ConvertirMiTweet(tweets);


		}


              

	}
}
