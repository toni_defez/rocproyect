﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace RocProyect.Droid.Fragments
{
	public class PlaceHolderFragment : Android.Support.V4.App.Fragment
    {
        private static String ARG_SECTION_NUMBER = "section_number";

        public PlaceHolderFragment()
        {
        }

        public static PlaceHolderFragment newInstance(int sectionNumber)
        {
            PlaceHolderFragment fragment = new PlaceHolderFragment();
            Bundle args = new Bundle();
            args.PutInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.Arguments = args;
            return fragment;
        }

    }
}
