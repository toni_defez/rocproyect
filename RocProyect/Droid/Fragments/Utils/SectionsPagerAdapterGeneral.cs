﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace RocProyect.Droid.Fragments
{
	public class SectionsPagerAdapterGeneral : FragmentPagerAdapter
    {
        private List<BaseFragment> items;

        public SectionsPagerAdapterGeneral(Android.Support.V4.App.FragmentManager fm,
                                            List<BaseFragment> items) : base(fm)
        {
            this.items = items;
        }

        protected SectionsPagerAdapterGeneral(IntPtr javaReference,
                                               JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override int Count => items.Count;

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            if (position == 0)
                return (items[0]);

            else if (position == 1)
                return (items[1]);

            else if (position == 2)
                return (items[2]);

            return PlaceHolderFragment.newInstance(position + 1);
        }
    }
}
