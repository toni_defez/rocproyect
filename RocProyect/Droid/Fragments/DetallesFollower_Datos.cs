﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Droid.Fragments
{
    public class DetallesFollower_Datos : BaseFragment
    {
        View _rootView;
        private DetalleFollower _detailFollower;
        private Follower _follower;

        private TextView _nombreUsuario;

        private ImageView _imagenFollower;
        private TextView _estadoFollower;


        //para los amigos
        private LinearLayout _linearFriends;
        private List<Follower> _listFriends;


        //para los seguidores
        private LinearLayout _linearFollower;
        private List<Follower> _listFollowers;


        //contadores amigos/Seguidores
        private TextView _contFriends;
        private TextView _contFollower;
		private TextView _countLikes;
		private TextView _countRetweets;

        private Twitter _twitter;
		private ProgressDialog _progress;


        //swipper
		protected ViewPager _viewPager;
		protected List<MiTweet> _listTweetsFavoritos;
		protected List<SimpleTweetFragment> _listFragmentFavoritos;
		protected FragmentPagerAdapter _tweetFragmentPagerAdapter;
		protected bool handled;
		protected int numElements;






        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			_rootView = inflater.Inflate(Resource.Layout.Detalles_Follower_FragmentDatos,
			 container, false);
			_detailFollower = (DetalleFollower)this.Activity;
			_viewPager = (ViewPager)_rootView.FindViewById(Resource.Id.viewPagerSlider);
			_follower = _detailFollower.Follower;

			_twitter = Twitter.getInstance;


			InitComponents();

			Glide.With(this.Context).Load(_follower.Imagen)
				 .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform())
				 .Into(_imagenFollower);

			_estadoFollower.Text = _follower.estado;
			_nombreUsuario.Text = _follower.Nombre;
			_contFriends.Text = _follower.ContFriends + "";
			_contFollower.Text = _follower.ContSeguidores + "";


			_countLikes.Text = _follower.ContFav + "";
         
			_progress = new ProgressDialog(this.Context);

			CargarDatosUsuario();

			return _rootView;
		}

		private async void CargarDatosUsuario()
		{
			_progress.SetMessage("Cargando...");
			_progress.Show();
			await CargarTweetsFavoritosAsync();
			await IniciarListaFollowerAsync();
			await IniciarListaFriendsAsync();
			_progress.Dismiss();
		}

		private async Task CargarTweetsFavoritosAsync()
		{
			List<MiTweet> tmp =  await _twitter.ObtenerTweetsFavoritosAsync(_follower);
            if(tmp != null )
			{
				numElements = tmp.Count > 5 ? 5 : tmp.Count;
				_listTweetsFavoritos = tmp.GetRange(0, numElements);
                _tweetFragmentPagerAdapter = new FragmentPagerAdapter(ChildFragmentManager);

                foreach (var tweet in _listTweetsFavoritos)
                {
					await Task.Run(() => { createTweetFragment(tweet); });

                    
                }

                _viewPager.Adapter = _tweetFragmentPagerAdapter;
			}

                    
		}

		private void createTweetFragment(MiTweet tweet)
		{
			_tweetFragmentPagerAdapter.addFragmentView((arg1, arg2, arg3) =>
            {
            
				var view = arg1.Inflate(Resource.Layout.Tweet_CardView, arg2, false);

                //elementos elimindos que no nos interesan
				var linearBotones = view.FindViewById<LinearLayout>(Resource.Id.linearLayout1);
				linearBotones.Visibility = ViewStates.Gone;
				var Imagen = view.FindViewById<ImageView>(Resource.Id.imageTweet);
				Imagen.Visibility = ViewStates.Gone;
                
                //elementos que nos interesan
				ImageView imagePerfil = view.FindViewById<ImageView>(Resource.Id.imagen_User);
				TextView nombreUser = view.FindViewById<TextView>(Resource.Id.nombre_User);
				TextView texto = view.FindViewById<TextView>(Resource.Id.txt_tweet);
				TextView fecha = view.FindViewById<TextView>(Resource.Id.txt_fecha);
                
                //rellenando valores
				Glide.With(this.Context).Load(tweet.imagen_usuario)
                 .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform())
				     .Into(imagePerfil);

				nombreUser.Text = tweet.nombre_usuario;
				texto.Text = tweet.text;
				fecha.Text = tweet.time;

				// Devolviendo la view
				CambiarGestoPager();


                return view;
            });
		}

		private void CambiarGestoPager()
		{
			 _viewPager.Touch += (s, e) => {


                if (e.Event.Action == MotionEventActions.Down)
                {
                    handled = false;
                }
                if (e.Event.Action == MotionEventActions.Move)
                {
                    handled = true;
                }

                if (e.Event.Action == MotionEventActions.Up)
                {
                    if (handled == false)
                    {
						MiTweet choice = _listTweetsFavoritos[_viewPager.CurrentItem];
						Intent intent = new Intent(this.Context, typeof(DetallesTweetActivity));
                         intent.PutExtra("detallesTweet",
						                 JSON.SerializeJSON(choice));
						this.Activity.StartActivityForResult(intent, 0);
                        StartActivity(intent);
                    }
                    handled = true;
                }
                
                e.Handled = false;
            };
		}

		private async Task IniciarListaFriendsAsync()
        {
			_listFriends= await _twitter.ObtenerFriendsAsync(_follower);


			if (_listFriends != null)
			{
				foreach (Follower follower in _listFriends)
				{
					 _linearFriends.AddView(insertarFoto(follower.Imagen));

				}
			}

        }

        private async Task IniciarListaFollowerAsync()
        {
			_listFollowers = await _twitter.ObtenerFollowerAsync(_follower);

			if(_listFollowers != null)
			{
				foreach (Follower follower in _listFollowers)
                {
					_linearFollower.AddView(insertarFoto(follower.Imagen)); 
                }
			}
           
        }

        private View insertarFoto(string url)
        {
            ImageView image = new ImageView(this.Context);
			image.SetMaxWidth(180);
			image.SetMinimumWidth(180);
			image.SetMaxHeight(180);
			image.SetMinimumHeight(180);
            image.SetScaleType(ImageView.ScaleType.FitCenter);
                     
			Glide.With(this.Context).Load(url)
				     .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform()
			                )
			         .Into(image);
			
            LinearLayout layout = new LinearLayout(this.Context);

            layout.AddView(image);
            return layout;
        }

        private void InitComponents()
        {
            _nombreUsuario = (TextView)_rootView.FindViewById(Resource.Id.nombreUsuario);
            _estadoFollower = (TextView)_rootView.FindViewById(Resource.Id.estado_follower);
            _contFriends = (TextView)_rootView.FindViewById(Resource.Id.contFriends);
            _contFollower = (TextView)_rootView.FindViewById(Resource.Id.contSeguidores);

			_countLikes = (TextView)_rootView.FindViewById(Resource.Id.countLikes);
			 _linearFriends = (LinearLayout)_rootView.FindViewById(Resource.Id.mygalleryFriends);
            _linearFollower = (LinearLayout)_rootView.FindViewById(Resource.Id.mygalleryFollower);
            _imagenFollower = (ImageView)_rootView.FindViewById(Resource.Id.imagePerfil);
        }
    }
}
