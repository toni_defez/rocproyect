﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Models;
using RocProyect.Droid.Utils;
using RocProyect.Models;
using RocProyect.Services;

namespace RocProyect.Droid.Fragments
{
	public class EditarPerfil_Fragment : BaseFragment
	{
		private View _rootView;
		private EditText _txtEmail;
		private EditText _txtPass;
		private EditText _txtPass2;

		private Button _btnlogin;
		private ImageView _btnBorrar;
		private ImageView _imagenUsuario;
		private Twitter _twitter;
		private UserNode _keys;

		private IUserService _serviceUser;


		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			_rootView = inflater.Inflate(Resource.Layout.ConfiguracionFragment,
			  container, false);                            
			IniciarComponentes();
			RellenarCampos();
			IniciarBotones();
			return _rootView;
		}

		private void RellenarCampos()
		{
			Glide.With(Context).Load(_twitter.user.ProfileImageUrl400x400)
							.Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform()
								   .Placeholder(Resource.Mipmap.load).Error(Resource.Mipmap.cancel))
							 .Into(_imagenUsuario);
			_keys = Constants.userNode;
			_serviceUser = new UserService();

			_txtEmail.Text = _keys.Email;
			_txtPass.Text = _keys.Password;
			_txtPass2.Text = _keys.Password;
		}

		private void IniciarComponentes()
		{
			_txtEmail = (EditText)_rootView.FindViewById(Resource.Id.txt_nombre);
			_txtPass = (EditText)_rootView.FindViewById(Resource.Id.txt_apellidos);
			_txtPass2 = (EditText)_rootView.FindViewById(Resource.Id.txt_email);
			_imagenUsuario = (ImageView)_rootView.FindViewById(Resource.Id.imageView1);
			_twitter = Twitter.getInstance;
			_btnlogin = (Button)_rootView.FindViewById(Resource.Id.btn_login);
			_btnBorrar = (ImageView)_rootView.FindViewById(Resource.Id.imgBasura);
		}

		private void IniciarBotones()
		{
			_btnlogin.Click += async delegate
			{

				if (_txtEmail.Length() == 0 || _txtPass.Length() == 0
				   || _txtPass2.Length() == 0)
				{
					Toast.MakeText(this.Context, "No se puede dejar ningun campo vacío", ToastLength.Long).Show();
				}
				else if (!IsValidEmail(_txtEmail.Text))
				{
					Toast.MakeText(this.Context, "El email no es valido", ToastLength.Long).Show();
				}
				else if (_txtPass2.Text != _txtPass.Text)
				{
					Toast.MakeText(this.Context, "Las dos contraseñas no coinciden ",
								   ToastLength.Long).Show();
				}
				else
				{
					string email = _txtEmail.Text;
					string pass = _txtPass.Text;
					ProgressDialog tmp = new ProgressDialog(this.Context);
					tmp.SetMessage("Modificando perfil....");
					var resultado = await _serviceUser.GetEditarUsuarioResponseAsync(
						email, pass, Constants.userNode.Token);
					tmp.Dismiss();
					if (resultado != null)
					{
						Toast.MakeText(this.Context, "La modificacion ha sido un exito ",
								   ToastLength.Long).Show();

						Constants.userNode.Email = email;
						Constants.userNode.Password = pass;
						UserCredentials.Remove(this.Context);
						UserCredentials.SaveUserLogin(Constants.userNode, this.Context);
					}

				}
			};

			_btnBorrar.Click += delegate
			{

				AlertDialog.Builder dialog = new
					AlertDialog.Builder(this.Activity);
				AlertDialog alert = dialog.Create();
				alert.SetTitle("Borrar cuenta rocProyect");
				alert.SetMessage("¿Estas seguro de querer borrar al usuario?");
				alert.SetIcon(Resource.Mipmap.ayuda50);

				alert.SetButton("Si", (c, ev) =>
				{
					var resultado = _serviceUser.GetDeleteUserAsync(Constants.userNode.Token,
																	Constants.userNode.ID);
					if (resultado != null)
					{
						Intent intent = new Intent(this.Context, typeof(LoginActivity));
						StartActivity(intent);
					}

				});

				alert.SetButton2("No", (c, ev) => { });
				alert.Show();

			};
		}

		private Boolean IsValidEmail(String email)
		{
			return Android.Util.Patterns.EmailAddress.Matcher(email).Matches();
		}
	}
}
