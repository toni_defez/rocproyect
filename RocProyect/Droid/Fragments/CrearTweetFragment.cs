﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Models;
using Tweetinvi;
using System.Globalization;
using RocProyect.Services;
using RocProyect.Models;
using RocProyect.Utils;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Utils;

namespace RocProyect.Droid.Fragments
{
	/// <summary>
	/// Pantalla donde podemos ver el Crear tweets , Editar tweets y
	/// crear tweets programados
	/// </summary>
	public class CrearTweetFragment : BaseFragment, View.IOnClickListener
	{
		private View _rootView;
		private EditText _tweetTexto;
		private Button _btnEnviar;
		private Twitter _twitter;
		private TextView _contador;
		private TextView _titulo;
		private NodeTweet _tweetModicar;

		//para los tweets programados
		private Button _btnFecha;
		private Button _btnHora;
		private TextView _textHora;
		private TextView _textFecha;
		private CheckBox _ckProgramado;

		private LinearLayout _contenedorTweetProgramado;
		private bool _esProgramado;
		private bool _esEditado;
		private ITweetService _tweetService;

		TimePickerDialog dialogTime;
		DatePickerDialog dialogDate;

		//para los calendarios
		private int dia, mes, anyo, hora, minuto;
		private bool vezPermitido;

		public NodeTweet TweetModicar { get => _tweetModicar; set => _tweetModicar = value; }

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			_twitter = Twitter.getInstance;
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			IniciarComponentes(inflater, container);

			if (TweetModicar != null)
			{
				_titulo.Text = "Modificando Tweet";
				_esEditado = true;
				_ckProgramado.Visibility = ViewStates.Gone;
				_btnEnviar.Text = "Modificar Tweet";
				_contenedorTweetProgramado.Visibility = ViewStates.Visible;
				vezPermitido = true;
				iniciarFechas();
				vezPermitido = false;
				_esProgramado = false;

			}
			else
			{
				_titulo.Text = "Crear nuevo Tweet";
				_esEditado = false;
			}


			_btnHora.SetOnClickListener(this);
			_btnFecha.SetOnClickListener(this);

			if (!_esEditado)
			{
				_ckProgramado.CheckedChange += delegate
				{

					_esProgramado = !_esProgramado;
					if (_esProgramado)
					{
						_contenedorTweetProgramado.Visibility = ViewStates.Visible;
						_btnEnviar.Text = "Guardar";
					}

					else
					{
						_contenedorTweetProgramado.Visibility = ViewStates.Gone;
						_btnEnviar.Text = "Enviar";
					}
				};
			}

			_tweetTexto.EditorAction += delegate (Object sender, TextView.EditorActionEventArgs e)
			{

				if (e.ActionId.Equals(global::Android.Views.InputMethods.ImeAction.Done))
				{
					KeyboardUtils.HideKeyboard(this.Activity);
				}
			};
			InciarBotones();         
			return _rootView;
		}

		private void InciarBotones()
		{
			_btnEnviar.Click += async delegate
			{
				string texto = _tweetTexto.Text;
				if (texto.Length < 140)
				{
					if (_esEditado)
					{
						var result = await _tweetService.GetUpdateTweetAsync(
												   Constants.userNode.Token, texto, _textFecha.Text, _textHora.Text,
													TweetModicar._id);
						if (result != null)
						{
							TweetModicar = null;
							Toast.MakeText(this.Context, "El tweet ha sido modificado"
							, ToastLength.Long).Show();
						}

					}               
					else if (_esProgramado)
					{
						if (_textHora.Length() == 0 || _textFecha.Length() == 0)
							Toast.MakeText(this.Context, "Seleccione la fecha y la hora para la publicación del tweet"
							  , ToastLength.Long).Show();                  
						else
						{

							var result = await _tweetService.GetNewTweetAsync(Constants.userNode.Token, texto, _textFecha.Text, _textHora.Text);

							if (result != null)
								Toast.MakeText(this.Context, "El tweet ha sido programado"
							  , ToastLength.Long).Show();
						}
					}               
					else
					{
						_twitter.HacerNuevoTweet(texto);
						Toast.MakeText(this.Context, "El tweet ha sido enviado"
							  , ToastLength.Long).Show();
					}
				}
			};

			_tweetTexto.TextChanged += delegate
			{

				string texto = _tweetTexto.Text;
				_contador.Text = texto.Length + "";
				if (texto.Length > 140)
					_contador.SetTextColor(Android.Graphics.Color.Red);
				else
					_contador.SetTextColor(Android.Graphics.Color.Blue);

			};
		}

		private void IniciarComponentes(LayoutInflater inflater, ViewGroup container)
		{
			_rootView = inflater.Inflate(Resource.Layout.CrearTweetFragment,
									 container, false);

			_tweetTexto = (EditText)_rootView.FindViewById(Resource.Id.tweetCampo);
			_btnEnviar = (Button)_rootView.FindViewById(Resource.Id.btn_enviarTweet);
			_contador = (TextView)_rootView.FindViewById(Resource.Id.contador);

			_btnHora = (Button)_rootView.FindViewById(Resource.Id.btnHora);
			_btnFecha = (Button)_rootView.FindViewById(Resource.Id.btnFecha);
			_textHora = (TextView)_rootView.FindViewById(Resource.Id.textHora);
			_textFecha = (TextView)_rootView.FindViewById(Resource.Id.textFecha);
			_ckProgramado = (CheckBox)_rootView.FindViewById(Resource.Id.ckProgramado);
			_contenedorTweetProgramado = (LinearLayout)_rootView.FindViewById(Resource.Id.ContenedorTweetProgramado);
			_contenedorTweetProgramado.Visibility = ViewStates.Gone;
			_esProgramado = false;
			_esEditado = false;
			_tweetService = new TweetService();
			_titulo = (TextView)_rootView.FindViewById(Resource.Id.NombreSeccion);
			vezPermitido = false;
		}

		private void iniciarFechas()
		{
			DateTime dt = DateTime.ParseExact(TweetModicar.fecha, "dd/MM/yyyy"
											  , CultureInfo.InvariantCulture);
			DateTime dto = DateTime.ParseExact(TweetModicar.hora, "HH:mm",
											   CultureInfo.InvariantCulture);
			IniciarTimePicker(dto);
			IniciarDatePicker(dt);

			_textFecha.Text = TweetModicar.fecha;
			_textHora.Text = TweetModicar.hora;
			_tweetTexto.Text = TweetModicar.text;

		}

		public void OnClick(View v)
		{
			switch (v.Id)
			{
				case Resource.Id.btnHora:
					IniciarTimePicker(DateTime.Now);
					break;
				case Resource.Id.btnFecha:
					IniciarDatePicker(DateTime.Today);
					break;
			}
		}

		private void IniciarTimePicker(DateTime today)
		{

			dialogTime = new TimePickerDialog(this.Context, OnDateSetHora, today.Hour, today.Minute, true);
			if (!vezPermitido)
				dialogTime.Show();
		}

		private void IniciarDatePicker(DateTime today)
		{

			dialogDate = new DatePickerDialog(this.Context, OnDateSetFecha, today.Year, today.Month - 1, today.Day);
			dialogDate.DatePicker.MinDate = today.Millisecond;
			if (!vezPermitido)
				dialogDate.Show();
		}

		private void OnDateSetFecha(object sender, DatePickerDialog.DateSetEventArgs e)
		{
			string dia, mes;
			dia = mes = "";
			dia = (e.DayOfMonth < 9) ? "0" + e.DayOfMonth : e.DayOfMonth + "";
			mes = (e.Month + 1 < 9) ? "0" + (e.Month + 1) : e.DayOfMonth + "";

			_textFecha.Text = dia + "/" + mes + "/" + e.Year;
		}

		private void OnDateSetHora(object sender, TimePickerDialog.TimeSetEventArgs e)
		{
			string hora = (e.HourOfDay < 9) ? "0" + e.HourOfDay : e.HourOfDay + "";
			string minuto = (e.Minute < 9) ? "0" + e.Minute : e.Minute + "";
			_textHora.Text = hora + ":" + minuto;
		}

	}
}
