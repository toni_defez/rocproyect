﻿
using System;
using Fragment = Android.Support.V4.App.Fragment;

namespace RocProyect.Droid.Fragments
{
	/// <summary>
	/// Fragmento Base para conectar todos los fragment presentes en la
	/// aplicacion 
	/// </summary>
	public class BaseFragment : Fragment
    {
        public BaseFragment()
        {
        }
        
        public interface OnFragmentInteractionListener
        {
            void OnFragmentInteracion(Uri uri);
        }

    }
}
