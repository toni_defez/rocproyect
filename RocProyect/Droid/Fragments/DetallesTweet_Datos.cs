﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Android.App;
using Android.OS;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;

namespace RocProyect.Droid.Fragments
{
	public class DetallesTweet_Datos : BaseFragment
	{
		View _rootView;
		private ImageView _userImagen;
		private TextView _nombreUser;
		private TextView _txtTwitter;
		private TextView _txtDate;
		private ImageButton _btnLike;
		private ImageButton _btnReTweet;
		private ImageButton _btnReply;
		private ImageView _imageTweet;
		private LinearLayout _userRetweets;
		private List<Follower> _listFollowers;
		private LinearLayout _linearFotos;
		private MiTweet _tweet;
		private Twitter _twitter;
		private TextView _countLike;
		private TextView _countRetwwet;
		private DetallesTweetActivity _activity;
		private FragmentPagerAdapter imageFragmentPagerAdapter;
		private ViewPager _viewPager;


		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			_rootView = inflater.Inflate(Resource.Layout.DetallesTweet, container, false);
			_activity = (DetallesTweetActivity)this.Activity;

			_tweet = _activity.Tweet;
			_twitter = Twitter.getInstance;
			InitFields();


			UpdateFieldsAsync();
			return _rootView;
		}

		private void InitFields()
		{
			_userImagen = (ImageView)_rootView.FindViewById(Resource.Id.imagen_User);
			_nombreUser = (TextView)_rootView.FindViewById(Resource.Id.nombre_User);
			_txtTwitter = (TextView)_rootView.FindViewById(Resource.Id.txt_tweet);
			_txtDate = (TextView)_rootView.FindViewById(Resource.Id.txt_fecha);
			_btnLike = (ImageButton)_rootView.FindViewById(Resource.Id.btn_like);
			_btnReply = (ImageButton)_rootView.FindViewById(Resource.Id.btn_reply);
			_btnReTweet = (ImageButton)_rootView.FindViewById(Resource.Id.btn_retweet);
			_imageTweet = (ImageView)_rootView.FindViewById(Resource.Id.imageTweet);
			_userRetweets = (LinearLayout)_rootView.FindViewById(Resource.Id.mygallery);

			_viewPager = (ViewPager)_rootView.FindViewById(Resource.Id.viewPagerSlider);

			_countLike = (TextView)_rootView.FindViewById(Resource.Id.countlike);
			_countRetwwet = (TextView)_rootView.FindViewById(Resource.Id.countretweet);
		}

		private async void UpdateFieldsAsync()
		{

			if (_tweet.favourites != 0)
				_countLike.Text = "Like:" + _tweet.favourites;
			else
				_countLike.Visibility = ViewStates.Gone;


			if (_tweet.retweets != 0)
				_countRetwwet.Text = "Retweet:" + _tweet.retweets;
			else
				_countRetwwet.Visibility = ViewStates.Gone;


			IniciarBotones();

			//poniendo valores 
			Glide.With(this).Load(_tweet.imagen_usuario)
				 .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform()
						.Placeholder(Resource.Mipmap.load).Error(Resource.Mipmap.cancel))
				 .Into(_userImagen);
			_nombreUser.Text = _tweet.nombre_usuario;
			_txtTwitter.Text = _tweet.text;



			//Obteniendo a los usuarios que han hecho retweet 
			await UsuariosRetweet();

			//Mostrando todas las fotos del usuario
			MostrarFotos();
		}

        /// <summary>
        /// Creamos el slider que contendra las fotos del tweet
        /// </summary>
		private void MostrarFotos()
		{
			if (_tweet.imagen_tweet != null)
			{

				imageFragmentPagerAdapter = new FragmentPagerAdapter(ChildFragmentManager);

				foreach (string url in _tweet.list_imagenes)
				{
					AgregarFotoSlider(url);
				}
				_viewPager.Adapter = imageFragmentPagerAdapter;
			}
			else
			{
				TextView galeria = (TextView)_rootView.FindViewById(Resource.Id.galeriaFotos);
				galeria.Visibility = ViewStates.Gone;

			}
		}

        /// <summary>
        /// Dibujamos la serie de usuarios que nos han dado a retweet
        /// </summary>
        /// <returns>The retweet.</returns>
		private async System.Threading.Tasks.Task UsuariosRetweet()
		{
			if (_tweet.retweets != 0)
			{
				_listFollowers = await _twitter.UsuarioRetweetAsync(_tweet);
				if (_listFollowers != null)
				{
					foreach (Follower follower in _listFollowers)
					{
						_userRetweets.AddView(InsertarFollower(follower));

					}
				}
			}
			else
			{
				TextView usuarios = (TextView)_rootView.FindViewById(Resource.Id.usuariosRetweet);
				usuarios.Visibility = ViewStates.Gone;
			}
		}



		private void IniciarBotones()
		{
			if (_tweet.favorito)
				_btnLike.SetImageResource(Resource.Mipmap.like_lleno);
			else
				_btnLike.SetImageResource(Resource.Mipmap.like_vacio);

			if (_tweet.retweet)
				_btnReTweet.SetImageResource(Resource.Mipmap.retweet_lleno);
			else
				_btnReTweet.SetImageResource(Resource.Mipmap.retweet_vacio);

			if (_tweet.contestado)
				_btnReply.SetImageResource(Resource.Mipmap.reply_lleno);
			else
				_btnReply.SetImageResource(Resource.Mipmap.reply_vacio);


			_btnLike.Click += delegate
			{
				_twitter.HacerFavorito(_tweet);
				_btnLike.SetImageResource(Resource.Mipmap.like_lleno);
				_tweet.favorito = true;

			};

			_btnReply.Click += delegate
			{
				EnviarMensaje(_tweet, _btnReply);
			};


			_btnReTweet.Click += delegate
			{
				_twitter.HacerRetweet(_tweet);
				_btnReTweet.SetImageResource(Resource.Mipmap.retweet_lleno);
				_tweet.retweet = true;
			};
		}

		private void EnviarMensaje(MiTweet tweet, ImageButton btnReply)
		{
			LayoutInflater layoutInflater = LayoutInflater.From(this.Activity);
			View mView = layoutInflater.Inflate(Resource.Layout.AlertDialogEditText, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(this.Activity);
			builder.SetView(mView);

			EditText email = mView.FindViewById<EditText>(Resource.Id.campoMensaje);

			AlertDialog alertDialog = null;
			builder.SetCancelable(false).SetPositiveButton("Send", async delegate
			{
				if (email.Length() != 0)
				{
					ProgressDialog tmp = new ProgressDialog(this.Activity);
					tmp.SetMessage("Enviando respuesta...");
					tmp.Show();
					bool contestado = await _twitter.ContestarTweetAsync(tweet, email.Text);

					if (contestado)
					{
						tweet.contestado = true;
						btnReply.SetImageResource(Resource.Mipmap.reply_lleno);
					}
					tmp.Dismiss();
				}

			});
			builder.SetNegativeButton("Cerrar", delegate
			{
				alertDialog.Dismiss();
			});

			alertDialog = builder.Create();
			alertDialog.Show();

		}

        /// <summary>
        /// Agregamos una nuevo foto al slider 
        /// </summary>
        /// <param name="url">URL.</param>
		private void AgregarFotoSlider(string url)
        {
            imageFragmentPagerAdapter.addFragmentView((arg1, arg2, arg3) =>
            {

                var view = arg1.Inflate(Resource.Layout.ImageViewLayout, arg2, false);
                ImageView image = view.FindViewById<ImageView>(Resource.Id.imageView);
                Glide.With(this.Context).Load(url)
                     .Into(image);

                return view;
            });
        }
        
        /// <summary>
        /// Insetamos la foto del usuario 
        /// </summary>
        /// <returns>The follower.</returns>
        /// <param name="follower">Follower.</param>
		private View InsertarFollower(Follower follower)
		{
			ImageView image = new ImageView(this.Context);
			image.SetScaleType(ImageView.ScaleType.FitCenter);

			Glide.With(this).Load(follower.Imagen)
				 .Apply(Com.Bumptech.Glide.Request.RequestOptions.CircleCropTransform()
						.Placeholder(Resource.Mipmap.load).Error(Resource.Mipmap.cancel))
				 .Into(image);

			LinearLayout layout = new LinearLayout(this.Context);
			LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(250, 250);
			linearLayoutParams.SetMargins(0, 0, 0, 0);
			layout.LayoutParameters = linearLayoutParams;

			image.Click += delegate
			{
				Toast.MakeText(this.Context, follower.Nombre
						, ToastLength.Long).Show();
			};
			layout.AddView(image);
			return layout;

		}
	}
}
