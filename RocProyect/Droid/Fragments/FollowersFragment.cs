﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Models;
using RocProyect.Utils;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Models;
using Tweetinvi;

namespace RocProyect.Droid.Fragments
{
	/// <summary>
    /// Pantalla donde aparece un listado con todos nuestros seguidores
    /// </summary>
	public class FollowersFragment : BaseFragment
    {
        private RecyclerView _recycler;
        private  FollowerAdapter _adapter;
        private View _root;
        private List<Follower> _listFollowers =new List<Follower>();
        private GridLayoutManager _manager;
        private Twitter _twitter = Twitter.getInstance;
        private ProgressDialog _pr;

		private ArrayAdapter<string> _adapterString;
		private List<string> _listnombresUsuario;
		private AutoCompleteTextView _buscadorUsuarios;

        internal class CustomSpanSize : GridLayoutManager.SpanSizeLookup
        {

            internal List<Follower> _listarExpedientes;
            internal CustomSpanSize(List<Follower> listarExpedientes)
            {

                _listarExpedientes = listarExpedientes;
            }

            public override int GetSpanSize(int position)
            {
                    return 1;
            }
        }


		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _root = inflater.Inflate(Resource.Layout.FollowersFragment,
              container, false);

			var stream = Stream.CreateUserStream();
            stream.FollowedByUser += async (sender, args) =>
            {
				_pr = new ProgressDialog(this.Context);
                _pr.SetMessage("Cargando seguidores recientes...");
                
				_listFollowers = await _twitter.ObtenerFollowersAsync();
				this.Activity.RunOnUiThread(() => {
					_pr.Show();
					CargarFollowersAsync();
					Constants.LIST_FOLLOWERS = _listFollowers;
                    _pr.Dismiss();
				});
				 

			};
			stream.StartStreamAsync();


            initFragmentAsync();
            return _root;
        }
              
        private  void CargarFollowersAsync()
        {
			_listnombresUsuario = _listFollowers.Select(Item => Item.Nombre).ToList<string>();
			_recycler = (RecyclerView)_root.FindViewById(Resource.Id.recyclerView1);
            _recycler.HasFixedSize = true;
            _manager = new GridLayoutManager(this.Context, 2);
            _manager.SetSpanSizeLookup(new CustomSpanSize(_listFollowers));

            IniciarDropDown();

            _recycler.SetLayoutManager(_manager);
            _adapter = new FollowerAdapter(this.Activity, _listFollowers);
            _adapter.ItemClick += OnItemClick;
            _recycler.SetAdapter(_adapter);
            
        }
      
        private  void initFragmentAsync()
		{
			_listFollowers = Constants.LIST_FOLLOWERS;
			 CargarFollowersAsync();

		}

		private void IniciarDropDown()
		{
			int layoutItemId = Android.Resource.Layout.SimpleDropDownItem1Line;
			_buscadorUsuarios = (AutoCompleteTextView)_root.FindViewById(Resource.Id.busqueda);
			_adapterString = new ArrayAdapter<string>(this.Context, layoutItemId, _listnombresUsuario);

			_buscadorUsuarios.Adapter = _adapterString;
            
			_buscadorUsuarios.ItemClick += autoTextView_ItemClick;
		}

		private void autoTextView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			AutoCompleteTextView autoText = (AutoCompleteTextView)sender;

            var name = autoText.Text;

			List<Follower> tmp = _listFollowers.Where(item => item.Nombre.Equals(name)).ToList();

			if(tmp!= null && tmp.Count != 0)
			{
				Follower item = (Follower)tmp[0];
				Utils.KeyboardUtils.HideKeyboard(this.Activity);
     
                Intent intent = new Intent(this.Activity, typeof(DetalleFollower));
                intent.PutExtra("detalleFollower",
                                JSON.SerializeJSON(item));
                this.Activity.StartActivityForResult(intent, 0);            
			}         
		}

	

        private void OnItemClick(object sender, int position)
        {
            Follower item = (Follower) _listFollowers[position];

            Intent intent = new Intent(this.Activity, typeof(DetalleFollower));
            intent.PutExtra("detalleFollower",
                            JSON.SerializeJSON(item));
            this.Activity.StartActivityForResult(intent, 0);
        } 

    }
}
