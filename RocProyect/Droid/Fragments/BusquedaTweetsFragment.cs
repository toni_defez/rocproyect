﻿using System.Collections.Generic;
using Android.OS;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Models;

namespace RocProyect.Droid.Fragments
{
	/// <summary>
	/// Busqueda de tweets donde aparece los resultados de la busqueda
	/// </summary>
	public class BusquedaTweetsFragment : BaseFragment
	{
		public View _rootView;
		private List<MiTweet> timeLineList;
		protected ListView _listViewTimeLine;
		protected TimeLineAdapter _timeLineAdapter;
		private TextView _textoPortada;
		protected Android.Support.V4.Widget.SwipeRefreshLayout _timeLineSwipeRefreshLayout;
		private Switch _switch;

		public List<MiTweet> TimeLineList
		{ get => timeLineList; set => timeLineList = value; }

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			_rootView = inflater.Inflate(Resource.Layout.TimeLineFragment,
			 container, false);
			_listViewTimeLine =
				_rootView.FindViewById<ListView>(Resource.Id.listViewTimeLine);
			_textoPortada = _rootView.FindViewById<TextView>(Resource.Id.txt_misPropiedades);
			_textoPortada.Text = "Tweets encontrados";
			_switch = _rootView.FindViewById<Switch>(Resource.Id.monitored_switch);
            _switch.Visibility = ViewStates.Gone;
			_timeLineSwipeRefreshLayout =
			   (Android.Support.V4.Widget.SwipeRefreshLayout)
			   _rootView.FindViewById(Resource.Id.timeLine_swipe_refresh_layout);

			if(TimeLineList!=null)
			    ActualizarListaAsync();
			_timeLineSwipeRefreshLayout.Refresh += delegate
			{

				_timeLineSwipeRefreshLayout.Refreshing = false;
			};

			return _rootView;
		}
        
		public void ActualizarListaAsync()
		{
			if (_listViewTimeLine != null && TimeLineList !=null)
			{
				_timeLineAdapter = new TimeLineAdapter(TimeLineList,
															this.Activity);
				_listViewTimeLine.Adapter = _timeLineAdapter;
			}
		}

		public override void OnResume()
        {
            base.OnResume();
            ActualizarListaAsync();
        }
        

	}
}
