﻿using System.Collections.Generic;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;
using Tweetinvi;

namespace RocProyect.Droid.Fragments
{
	/// <summary>
    /// Obtenemos todos los detalles del usuario
    /// </summary>
	public class DetallesFollower_TimeLine : BaseFragment
	{
		private View _rootView;
		private DetalleFollower _detailFollower;
		private Follower _follower;
		private TimeLineAdapter _timeLineAdapter;
		private Android.Support.V4.Widget.SwipeRefreshLayout _timeLineSwipeRefreshLayout;
		private List<MiTweet> _timeLineList;
		private ListView _listViewTimeLine;
		private ProgressDialog _pr;
		private Twitter _twitter;
		protected Switch _switch;
        protected bool _enVivo;
		protected TextView _portada;

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			_rootView = inflater.Inflate(Resource.Layout.Detalles_Follower_FragmentTimeLine,
			container, false);
			_detailFollower = (DetalleFollower)this.Activity;
			_follower = _detailFollower.Follower;
			_twitter = Twitter.getInstance;
			_rootView = inflater.Inflate(Resource.Layout.TimeLineFragment,
			 container, false);
			_portada = (TextView)_rootView.FindViewById(Resource.Id.txt_misPropiedades);
			_timeLineSwipeRefreshLayout =
				(Android.Support.V4.Widget.SwipeRefreshLayout)
				_rootView.FindViewById(Resource.Id.timeLine_swipe_refresh_layout);
			_switch = _rootView.FindViewById<Switch>(Resource.Id.monitored_switch);
			_listViewTimeLine =
				_rootView.FindViewById<ListView>(Resource.Id.listViewTimeLine);
			_portada.Text = "Ultimos tweets ";
			_pr = new ProgressDialog(this.Context);
			CargarTimeLineAsync();

			_timeLineSwipeRefreshLayout.Refresh += async delegate
			{
				await ActualizarListaAsync();
			};

            var stream = Stream.CreateUserStream();
            stream.TweetCreatedByAnyone += (sender, args) => {

				if (args.Tweet.CreatedBy.Id == _follower.id_follower
				    && _enVivo)
				{
					this.Activity.RunOnUiThread(async () =>
					{

						await ActualizarListaAsync();
					});
				}

            };
            stream.StartStreamAsync();


			_enVivo = true;
            _switch.CheckedChange += delegate (object sender,
			                                   CompoundButton.CheckedChangeEventArgs e) {

                if (!e.IsChecked)
                    _enVivo = false;
                else
                    _enVivo = true;
            };
                     
			return _rootView;
		}

		private async void CargarTimeLineAsync()
		{
			_pr.SetMessage("Cargando...");
			_pr.Show();
			await ActualizarListaAsync();
			_pr.Dismiss();
		}

		private async System.Threading.Tasks.Task ActualizarListaAsync()
		{
			_timeLineList = await _twitter.TimeLineFollowerAsync(_follower);
			_timeLineAdapter = new TimeLineAdapter(_timeLineList,
														this.Activity);
			_listViewTimeLine.Adapter = _timeLineAdapter;
			_timeLineSwipeRefreshLayout.Refreshing = false;
		}
	}
}
