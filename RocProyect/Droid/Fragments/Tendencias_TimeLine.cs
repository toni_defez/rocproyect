﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;
using System.Threading;
using System.Threading.Tasks;
namespace RocProyect.Droid.Fragments
{
	public class Tendencias_TimeLine : TimeLineFragment
    {
		public string Topic { get; set; }
		private TextView titulo;

       
		public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _rootView = inflater.Inflate(Resource.Layout.TimeLineFragment,
              container, false);

            _timeLineSwipeRefreshLayout =
                (Android.Support.V4.Widget.SwipeRefreshLayout)
                _rootView.FindViewById(Resource.Id.timeLine_swipe_refresh_layout);
			_switch = _rootView.FindViewById<Switch>(Resource.Id.monitored_switch);
			_switch.Visibility = ViewStates.Gone;
            _listViewTimeLine =
                _rootView.FindViewById<ListView>(Resource.Id.listViewTimeLine);
            _twitter = Twitter.getInstance;
            _pr = new ProgressDialog(this.Context);
                     
			titulo = (TextView)_rootView.FindViewById(Resource.Id.txt_misPropiedades);
			titulo.Text = "Tendencia:" + Topic;
         
            CargarTimeLineAsync();
            _timeLineSwipeRefreshLayout.Refresh += async delegate {

                await ActualizarListaAsync();
            };
            return _rootView;
        }
              

		protected new async  Task ActualizarListaAsync()
        {
			_timeLineList = await _twitter.BuscarTweetsPorTextoAsync(Topic);
            _timeLineAdapter = new TimeLineAdapter(_timeLineList,
                                                        this.Activity);
            _listViewTimeLine.Adapter = _timeLineAdapter;

            _timeLineSwipeRefreshLayout.Refreshing = false;
        }


		protected new async void CargarTimeLineAsync()
        {
            _pr.SetMessage("Cargando...");
            _pr.Show();
            await ActualizarListaAsync();
            _pr.Dismiss();

        }

    }
}
