﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Models;
using RocProyect.Services;
using RocProyect.Utils;
using Tweetinvi;

namespace RocProyect.Droid.Fragments
{
    public class TweetProgramadosFragment : BaseFragment
    {
        private View _rootView;
        private NodeTweetAdapter _adapter;

        //Variables públicas
        public int dia, mes, anyo;
        public DateTime fechaSeleccionada;
        public ListView listView;
        public Button[,] agenda;
        public ListView listaActividades;

        //Componentes de la vista
        public TextView cabeceraAgenda;
        public TextView prevMonth;
        public TextView nextMonth;

        public List<string> listaDias;    //los dias que se estan programados
        public List<NodeTweet> listaTweetsNode;  //TODOS LOS TWEETS PROGRAMADOS DEL USUARIO
        public List<NodeTweet> listaFechas;      //TODOS LOS TWEETS DE ESE MES

        //Fechas
        public DateTime fecha;
        public string month;
        public string year;
        public string day;
		private ProgressDialog _pr;

        //servicio
        private ITweetService tweetService;


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _rootView = inflater.Inflate(Resource.Layout.TweetProgramadosFragment,
              container, false);

            fecha = new DateTime();
            fecha = DateTime.Now;

            month = DateUtils.GetMonthString(fecha);
            day = DateUtils.GetDayNumber(fecha);
                    
			cabeceraAgenda = (TextView)_rootView.FindViewById(Resource.Id.cabeceraMesAnyo);
            prevMonth = (TextView)_rootView.FindViewById(Resource.Id.cabeceraPrev);
            nextMonth = (TextView)_rootView.FindViewById(Resource.Id.cabeceraPost);
                     
            ObtenerTweetNode();

                
			var stream = Stream.CreateUserStream();
			stream.MessageSent += (sender, args) =>
			{
				this.Activity.RunOnUiThread(() =>
				{               
					ObtenerTweetNode();
				});

			};
            stream.StartStreamAsync();
           
            return _rootView;
        }

        private async void ObtenerTweetNode()
        {         
			_pr = new ProgressDialog(this.Context);
			_pr.SetMessage("Cargando...");
            _pr.Show();
            tweetService = new TweetService();
            var list =await tweetService.GetListadoTweetUserAsync(Constants.userNode.Token);
            if (list != null)
            {
                listaTweetsNode = list.tweet;
            }
            else
                listaTweetsNode = new List<NodeTweet>();

			_pr.Dismiss();
			SetUpCalendar(fecha);
        }

        public void SetUpCalendar(DateTime currentDate)
        {
            int maxDays = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
            int firstDay = WeekDayToInt(DateUtils
                                        .GetDayOfTheWeek(new DateTime(currentDate.Year,
			                                                          currentDate.Month, 1).DayOfWeek));

            cabeceraAgenda.Text = month + " " + currentDate.Year.ToString();
            prevMonth.Text = "<";
            nextMonth.Text = ">";

            cabeceraAgenda.TextSize = 25.0f;
            prevMonth.TextSize = 25.0f;
            nextMonth.TextSize = 25.0f;

            prevMonth.Click += delegate { PrevMonth(); };

            nextMonth.Click += delegate { NextMonth(); };

            InitializeCalendarButtons();
        }

        public void InitializeCalendarButtons()
        {
            int id = 1;
            agenda = new Button[6, 7];

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    string buttonID = "btn" + id;
                    id++;

                    int resID = Resources.GetIdentifier(buttonID, "id", Context.PackageName);
                    agenda[i, j] = (Button)_rootView.FindViewById(resID);
                }
            }

            PaintCalendar();
        }

        public int WeekDayToInt(string weekday)
        {
            if (weekday == "Lunes")
                return 1;
            if (weekday == "Martes")
                return 2;
            if (weekday == "Miércoles")
                return 3;
            if (weekday == "Jueves")
                return 4;
            if (weekday == "Viernes")
                return 5;
            if (weekday == "Sábado")
                return 6;
            else
                return 7;
        }

        #region Cambio de mes
        /// <summary>
        /// Cambia a un mes posterior, repintando los botones
        /// </summary>
        public void NextMonth()
        {
            if (fecha.Month == 12)
            {
                fecha = new DateTime(fecha.Year + 1, 1, 1);
                month = DateUtils.GetMonthString(fecha);

                cabeceraAgenda.Text = month + " " + (fecha.Year).ToString();

                ResetButtons();
                PaintCalendar();
            }
            else
            {
                fecha = new DateTime(fecha.Year, fecha.Month + 1, 1);
                month = DateUtils.GetMonthString(fecha);

                cabeceraAgenda.Text = month + " " + (fecha.Year).ToString();

                ResetButtons();
                PaintCalendar();
            }
        }

        /// <summary>
        /// Cambia a un mes anterior, repintando los botones
        /// </summary>
        public void PrevMonth()
        {
            if (fecha.Month == 1)
            {
                fecha = new DateTime(fecha.Year - 1, 12, 1);
                month = DateUtils.GetMonthString(fecha);

                cabeceraAgenda.Text = month + " " + (fecha.Year).ToString();

                ResetButtons();
                PaintCalendar();
            }
            else
            {
                fecha = new DateTime(fecha.Year, fecha.Month - 1, 1);
                month = DateUtils.GetMonthString(fecha);

                cabeceraAgenda.Text = month + " " + (fecha.Year).ToString();

                ResetButtons();
                PaintCalendar();
            }
        }
        #endregion

        public void ResetButtons()
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    agenda[i, j].Text = "";
                    agenda[i, j].SetTextColor(Color.ParseColor("#ff5e5e5e"));
                  //  agenda[i, j].SetBackgroundResource(Resource.Mipmap.boton_agenda_blanco);
					agenda[i, j].SetBackgroundColor(Color.Transparent);
                }
            }
        }


		public void PaintCalendar()
		{
			int firstDay = WeekDayToInt(DateUtils
										.GetDayOfTheWeek(new DateTime(fecha.Year, fecha.Month, 1).DayOfWeek));
			bool first = true;
			int numDays = 1;
			int maxDays = DateTime.DaysInMonth(fecha.Year, fecha.Month);

			ObtenerTweetNodePorMes(fecha);  //le pasamos una fecha y obtenemos todos con ese mes

			for (int i = 0; i < 6; i++)
			{
				for (int j = 0; j < 7 && numDays <= maxDays; j++)
				{
					if (i == 0 && first)
					{
						j = firstDay - 1;
						first = false;
					}

					string numDaysString = "";

					if (numDays < 10) { numDaysString = "0" + numDays; }
					else { numDaysString = numDays.ToString(); }

					if (listaDias.Contains(numDaysString))
					{
						agenda[i, j].SetBackgroundResource(Resource.Mipmap.boton_agenda);
						agenda[i, j].SetTextColor(Color.White);

						agenda[i, j].Click += (sender, e) =>
						{
							Android.Support.V7.Widget.AppCompatButton btn = (Android.Support.V7.Widget.AppCompatButton)sender;
							DateTime fechita = new DateTime(fecha.Year, fecha.Month, int.Parse(btn.Text));
							ActividadesDia(fechita);
						};
					}
					agenda[i, j].Text = numDays.ToString();
					agenda[i, j].TextSize = 20.0f;
					agenda[i, j].SetTypeface(Typeface.Default, TypefaceStyle.Bold);

					numDays++;
				}
			}

		}


		private void ObtenerTweetNodePorMes(DateTime fecha)
        {
            List<NodeTweet> list = new List<NodeTweet>();
            listaDias = new List<string>();

            foreach(NodeTweet tweet in listaTweetsNode)
            {
                int mes, anyo, dia;
                string[] fechas = tweet.fecha.Split('/');
                dia = Convert.ToInt32(fechas[0]);
                mes = Convert.ToInt32(fechas[1]);
                anyo = Convert.ToInt32(fechas[2]);

                if(fecha.Month == mes)
                {
                    list.Add(tweet);
                    if (!listaDias.Contains(fechas[0]))
                        listaDias.Add(fechas[0]);
                }
            }
			if(listaDias.Count!=0)
            listaDias = listaDias.OrderBy(c => int.Parse(c)).ToList();
            listaFechas = list;
                                  
        }

        //ESTE METODO OBTIENE LOS TWEETS NECESARIOS Y LOS DIBUJA EN EL ADAPTER
        public void ActividadesDia(DateTime fechaSeleccionada)
        {
            listaActividades = (ListView)_rootView.FindViewById(Resource.Id.listaAgenda);
			List<NodeTweet> lista = new List<NodeTweet>();
			//formato de la fecha 6/4/2018
			string dia, mes;
			if (fechaSeleccionada.Day < 9)
				dia = "0" + fechaSeleccionada.Day;
			else
				dia = fechaSeleccionada.Day+"";

			if (fechaSeleccionada.Month + 1 < 9)
				mes = "0" + fechaSeleccionada.Month;
			else
				mes = fechaSeleccionada.Month+"";

			string fechaActual = dia + "/" + mes + "/" + fechaSeleccionada.Year;
			lista = listaFechas.Where(ite => ite.fecha == fechaActual).ToList<NodeTweet>();

            
			//List<String> listNombre =
            //  listPoblaciones.Where(item => item.id == number)
			_adapter = new NodeTweetAdapter(lista, this.Activity,this);
            listaActividades.Adapter = _adapter;
        }
        

        
        public async void ActualizarListasAsync()
		{

			var list = await tweetService.GetListadoTweetUserAsync(Constants.userNode.Token);
            if(list != null)
			{
				listaTweetsNode = list.tweet;
				ResetButtons();
                PaintCalendar();

			}
		}


    }
}
