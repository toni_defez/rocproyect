﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;

namespace RocProyect.Droid.Fragments
{
	public class DetallesTweet_TimeLine : TimeLineFragment
    {
		public MiTweet tweet { get; set; }
		private DetallesTweetActivity _activity;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _rootView = inflater.Inflate(Resource.Layout.TimeLineFragment,
              container, false);
			_activity = (DetallesTweetActivity)this.Activity;

            tweet = _activity.Tweet;
            _timeLineSwipeRefreshLayout =
                (Android.Support.V4.Widget.SwipeRefreshLayout)
                _rootView.FindViewById(Resource.Id.timeLine_swipe_refresh_layout);

            _listViewTimeLine =
                _rootView.FindViewById<ListView>(Resource.Id.listViewTimeLine);
            _twitter = Twitter.getInstance;
               _pr = new ProgressDialog(this.Context);

            CargarTimeLineAsync();    
           
			_timeLineSwipeRefreshLayout.Refresh += async delegate {

                await ActualizarListaAsync();
            };
          

            return _rootView;
        }

        protected new async Task ActualizarListaAsync()
        {
            _timeLineList = await _twitter.ObtenerTimeLineRespuesta(tweet);
            _timeLineAdapter = new TimeLineAdapter(_timeLineList,
                                                        this.Activity);
            _listViewTimeLine.Adapter = _timeLineAdapter;

            _timeLineSwipeRefreshLayout.Refreshing = false;
        }


        protected new async void CargarTimeLineAsync()
        {
            _pr.SetMessage("Cargando...");
            _pr.Show();
            await ActualizarListaAsync();
            _pr.Dismiss();

        }

        
    }
}
