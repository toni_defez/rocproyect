﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;
using static Android.Widget.AdapterView;

namespace RocProyect.Droid.Fragments
{
	public class TendenciasFragment : BaseFragment
    {
		public View _root { get; private set; }
		private Twitter _twitter;
		private TendenciasAdapter _adapter;
		private Android.Support.V4.Widget.SwipeRefreshLayout _tendenciasSwipeRefreshLayout;
		private List<Tendencia> _tendenciasList;
		private ListView _listViewTendencias;
		private ProgressDialog _pr;

		public override void OnCreate(Bundle savedInstanceState)
        {
			base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {         
			_root = inflater.Inflate(Resource.Layout.TendenciasFragment,
              container, false);
			//ObtenerListadoTrendingTopic();

			//swipeTendencias
			//listViewTendencias
			//tendencias_swipe_refresh_layout

			_listViewTendencias = (ListView)_root.FindViewById(Resource.Id.listViewTendencias);
			_tendenciasSwipeRefreshLayout = (Android.Support.V4.Widget.SwipeRefreshLayout)_root.FindViewById(
				Resource.Id.tendencias_swipe_refresh_layout);
			_pr = new ProgressDialog(this.Context);
			_twitter = Twitter.getInstance;

			CargarTendenciasAsync();

			_tendenciasSwipeRefreshLayout.Refresh+= async delegate {
			
				await ActualizarListaAsync();

			};
            return _root;
        }

		private async void CargarTendenciasAsync()
		{
			_pr.SetMessage("Cargando...");
            _pr.Show();
			await ActualizarListaAsync();
            _pr.Dismiss();
		}

		private  async Task ActualizarListaAsync()
		{
			_tendenciasList = await _twitter.ObtenerTrendingTopicAsyncSpainAsync();
			_adapter = new TendenciasAdapter(_tendenciasList,
                                                        this.Activity);
			_listViewTendencias.Adapter = _adapter;

			_listViewTendencias.ItemClick+=delegate (object sender, ItemClickEventArgs e) {

				Tendencia tendencia = _tendenciasList[e.Position];
				Tendencias_TimeLine tmp = new Tendencias_TimeLine();
				tmp.Topic = tendencia.Nombre;
				this.Activity.SupportFragmentManager.BeginTransaction().
                    Replace(Resource.Id.content_main, tmp)
                                      .Commit();            
			};
                     

			_tendenciasSwipeRefreshLayout.Refreshing = false;
		}

	
    }
}
