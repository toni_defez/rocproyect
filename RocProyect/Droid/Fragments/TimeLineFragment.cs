﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;
using System.Threading;
using System.Threading.Tasks;
using Tweetinvi;

namespace RocProyect.Droid.Fragments
{
    public class TimeLineFragment : BaseFragment
    {
		protected View _rootView;
		protected TimeLineAdapter _timeLineAdapter;
		protected Android.Support.V4.Widget.SwipeRefreshLayout _timeLineSwipeRefreshLayout;
		protected List<MiTweet> _timeLineList;
		protected ListView _listViewTimeLine;
		protected ProgressDialog _pr;
		protected Twitter _twitter;
		protected Switch _switch;
		protected bool _enVivo;
       

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            _rootView = inflater.Inflate(Resource.Layout.TimeLineFragment,
              container, false);
            
            _timeLineSwipeRefreshLayout =
                (Android.Support.V4.Widget.SwipeRefreshLayout)
                _rootView.FindViewById(Resource.Id.timeLine_swipe_refresh_layout);

            _listViewTimeLine =
                _rootView.FindViewById<ListView>(Resource.Id.listViewTimeLine);
            _twitter = Twitter.getInstance;
            _pr = new ProgressDialog(this.Context);

			_switch = _rootView.FindViewById<Switch>(Resource.Id.monitored_switch);

			IniciarLista();

            _timeLineSwipeRefreshLayout.Refresh += async delegate {
            
                await ActualizarListaAsync();
            };

			var stream = Stream.CreateUserStream();
            stream.TweetCreatedByAnyone += (sender, args) => {

				if(_enVivo)
                this.Activity.RunOnUiThread(async () => {

                    await ActualizarListaAsync();
                });

            };
            stream.StartStreamAsync();

			_enVivo = true;
			_switch.CheckedChange += delegate (object sender, CompoundButton.CheckedChangeEventArgs e) {
              
				if(!e.IsChecked)
				{
					_enVivo = false;
				}
				else
				{
					_enVivo = true;
				}
            };






            return _rootView;
        }

		private void IniciarLista()
		{
			_timeLineAdapter = new TimeLineAdapter(Constants.LIST_TWEET_TIMELINE,
                                                       this.Activity);
            _listViewTimeLine.Adapter = _timeLineAdapter;

            _timeLineSwipeRefreshLayout.Refreshing = false;
		}

		protected async Task ActualizarListaAsync()
        {
			_timeLineList= await _twitter.ObtenerTimeLineAsync();
            _timeLineAdapter = new TimeLineAdapter(_timeLineList,
                                                        this.Activity);
            _listViewTimeLine.Adapter = _timeLineAdapter;

            _timeLineSwipeRefreshLayout.Refreshing = false;
			Constants.LIST_TWEET_TIMELINE = _timeLineList;
        }
        

		protected async void CargarTimeLineAsync()
        {
            _pr.SetMessage("Cargando...");
            _pr.Show();
            await ActualizarListaAsync();
            _pr.Dismiss();

        }
        
    }
}
