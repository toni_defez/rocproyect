﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;

namespace RocProyect.Droid.Fragments
{
	public class Menciones : BaseFragment
    {
             
		protected View _rootView;
        protected TimeLineAdapter _timeLineAdapter;
        protected Android.Support.V4.Widget.SwipeRefreshLayout _timeLineSwipeRefreshLayout;
		protected List<MiTweet> _timeLineList;
        protected ListView _listViewTimeLine;
        protected ProgressDialog _pr;
        protected Twitter _twitter;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
			_rootView = inflater.Inflate(Resource.Layout.TimeLineFragment,
              container, false);

            _timeLineSwipeRefreshLayout =
                (Android.Support.V4.Widget.SwipeRefreshLayout)
                _rootView.FindViewById(Resource.Id.timeLine_swipe_refresh_layout);

            _listViewTimeLine =
                _rootView.FindViewById<ListView>(Resource.Id.listViewTimeLine);
            _twitter = Twitter.getInstance;
            _pr = new ProgressDialog(this.Context);

            CargarTimeLineAsync();
            _timeLineSwipeRefreshLayout.Refresh += async delegate {

                await ActualizarListaAsync();
            };
            return _rootView;
        }



        protected async Task ActualizarListaAsync()
        {
			_timeLineList = await _twitter.ObtenerMencionesAsync(); 
            _timeLineAdapter = new TimeLineAdapter(_timeLineList,
                                                        this.Activity);
            _listViewTimeLine.Adapter = _timeLineAdapter;

            _timeLineSwipeRefreshLayout.Refreshing = false;
            
        }


        protected async void CargarTimeLineAsync()
        {
            _pr.SetMessage("Cargando...");
            _pr.Show();
            await ActualizarListaAsync();
            _pr.Dismiss();

        }



    }
}
