﻿using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;
using static Android.Widget.AdapterView;

namespace RocProyect.Droid.Fragments
{
	/// <summary>
    /// Fragment donde podemos ver los resultados de la busqueda 
	/// de los follower
    /// </summary>
	public class BusquedaUsuariosFragment : BaseFragment
    {
		protected View _rootView;
		protected FollowersBusquedaAdapter _followersAdapter;
        protected Android.Support.V4.Widget.SwipeRefreshLayout _usuariosSwipeRefreshLayout;
		private List<Follower> timeLineList;
		protected ListView _listViewTimeLine;
        protected Twitter _twitter;

		public List<Follower> TimeLineList { get => timeLineList; set => timeLineList = value; }

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container,
		                                  Bundle savedInstanceState)
        {
			_rootView = inflater.Inflate(Resource.Layout.BusquedaFragment_Usuarios,
             container, false);
			_listViewTimeLine =
				_rootView.FindViewById<ListView>(Resource.Id.listViewUsuariosEncontrados);
			_twitter = Twitter.getInstance;

			_usuariosSwipeRefreshLayout = (Android.Support.V4.Widget.SwipeRefreshLayout)
				_rootView.FindViewById(Resource.Id.usuarios_swipe_refresh_layout);

			if(TimeLineList!=null)
			    ActualizarListaAsync();
			_usuariosSwipeRefreshLayout.Refresh += delegate {
				_usuariosSwipeRefreshLayout.Refreshing = false;
             };
                     
            return _rootView;
        }

		public  void ActualizarListaAsync()
        {
			if (_listViewTimeLine != null && TimeLineList != null)
			{
				_listViewTimeLine =
				_rootView.FindViewById<ListView>(Resource.Id.listViewUsuariosEncontrados);            

				_followersAdapter = new FollowersBusquedaAdapter(TimeLineList,
															this.Activity);
				_listViewTimeLine.Adapter = _followersAdapter;
                            
				_listViewTimeLine.ItemClick +=delegate (object sender, ItemClickEventArgs e) {

					Follower tmp = (Follower)timeLineList[e.Position];
					Intent intent = new Intent(this.Activity, typeof(DetalleFollower));
					intent.PutExtra("detalleFollower",
                            JSON.SerializeJSON(tmp));
                    this.Activity.StartActivityForResult(intent, 1);
				};
			}
        }

		public override void OnResume()
		{
			base.OnResume();
			ActualizarListaAsync();
		}

	}
}
