﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace RocProyect.Droid.Fragments
{
	public class SimpleTweetFragment : Android.Support.V4.App.Fragment
    {
		readonly Func<LayoutInflater, ViewGroup, Bundle, View> view;

		public SimpleTweetFragment(Func<LayoutInflater, ViewGroup, Bundle, View> view)
        {
            this.view = view;
        }
       
		public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            return view(inflater, container, savedInstanceState);
        }
              
    }
}
