﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RocProyect.Droid.Activities;
using RocProyect.Droid.Adapters;
using RocProyect.Droid.Models;
using RocProyect.Models;
using RocProyect.Utils;
using Tweetinvi;
using static Android.Widget.AdapterView;

namespace RocProyect.Droid.Fragments
{
	public class NotificacionesFragment : BaseFragment
    {

		private View _rootView;
		private Twitter _twitter;
		private List<Chat> misChats;

		protected NotificacionAdapter _timeLineAdapter;
        protected Android.Support.V4.Widget.SwipeRefreshLayout _timeLineSwipeRefreshLayout;
        protected ListView _listViewTimeLine;
        protected ProgressDialog _pr;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
          	_rootView = inflater.Inflate(Resource.Layout.NotificacionFragment,
             container, false);
			_twitter = Twitter.getInstance;

			_timeLineSwipeRefreshLayout =
               (Android.Support.V4.Widget.SwipeRefreshLayout)
               _rootView.FindViewById(Resource.Id.timeLine_swipe_refresh_layout);

            _listViewTimeLine =
                _rootView.FindViewById<ListView>(Resource.Id.listViewTimeLine);
            _twitter = Twitter.getInstance;
                     
			IniciarListaAsync();
            _timeLineSwipeRefreshLayout.Refresh += async delegate {
				misChats = await _twitter.ObtenerMensajeAsync();
				ActualizarListaAsync();
            };

            //Abrimos escuchar para que se actualize para cada mensaje recibido
            var stream = Stream.CreateUserStream();
            stream.MessageReceived += async (sender, args) => {
                misChats = await _twitter.ObtenerMensajeAsync();
				this.Activity.RunOnUiThread(() => {

					_pr = new ProgressDialog(this.Context);
                    _pr.SetMessage("Cargando mensajes...");
					_pr.Show();
                    ActualizarListaAsync();

                    _pr.Dismiss();
				});
               
            };
			stream.StartStreamAsync();

   
      
			return _rootView;
        }

		private  void ActualizarListaAsync()
		{
			
			_timeLineAdapter = new NotificacionAdapter(misChats,
                                                        this.Activity);
            _listViewTimeLine.Adapter = _timeLineAdapter;         
			_listViewTimeLine.ItemClick += delegate (object sender, ItemClickEventArgs e) {

				Chat item = (Chat)misChats[e.Position];
                Intent intent = new Intent(this.Activity, typeof(ListaMensajesActivity));
                intent.PutExtra("detalleChat",
                                JSON.SerializeJSON(item));
				this.Activity.StartActivityForResult(intent,1);
            };
         
            _timeLineSwipeRefreshLayout.Refreshing = false;
            
		}
        
		public  void IniciarListaAsync()
		{
			_pr = new ProgressDialog(this.Context);
            _pr.SetMessage("Cargando mensajes...");
			_pr.Show();
			misChats = Constants.LIST_CHATS;
			ActualizarListaAsync();

			_pr.Dismiss();
		}

	
    }
}
