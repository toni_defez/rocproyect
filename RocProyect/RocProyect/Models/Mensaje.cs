﻿using System;
namespace RocProyect.Models
{
    public class Mensaje
    {

		private long idSender;
		private string texto;
		private DateTime fecha;
		private long id;
		private Usuario sender;
              
		public Mensaje()
		{
			
		}
		public Mensaje(long idSender, string texto, DateTime fecha)
		{
			this.idSender = idSender;
			this.texto = texto;
			this.Fecha = fecha;
		}

		public long IdSender1 { get => idSender; set => idSender = value; }
		public string Texto { get => texto; set => texto = value; }
		public DateTime Fecha { get => fecha; set => fecha = value; }
		public long Id { get => id; set => id = value; }
		public Usuario Sender { get => sender; set => sender = value; }
	}
}
