﻿using System;
namespace RocProyect.Models
{
    public class LoginResponse : OkResponse
    {
        public string token { get; set; }
        public string userId { get; set; }
    }
}
