﻿using System;
namespace RocProyect.Models
{
    //este modelo se usa en 
     //CrearTweet
     //ModificarTweet
    public class TweetActionResponse:OkResponse
    {
        
        public NodeTweet tweet { get; set; }
    }
}
