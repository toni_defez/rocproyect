﻿using System;
namespace RocProyect.Models
{
    public class EditarUsuarioResponse:OkResponse
    {
        public DetallesUsuarioResponse user { get; set; }
    }
}
