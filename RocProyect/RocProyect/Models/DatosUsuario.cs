﻿using System;
namespace RocProyect.Models
{
    /*Datos que guardamos para mostrar en la interfaz de UserApp*/
    public class DatosUsuario
    {
        public String imagen { get; set; }
        public DateTime fecha_alta_twitter { get; set; }
        public int ContSeguidores { get; set; }
        public string descripcion { get; set; }
        public int ContAmigos { get; set; }
        public string lengua { get; set; }
        public string location { get; set; }
        public string nombre { get; set; }


        public DatosUsuario(string imagen, DateTime fecha_alta_twitter,
            int ContSeguidores, int ContAmigos, string descripcion, string lengua,
            string location)
        {
            this.imagen = imagen;
            this.fecha_alta_twitter = fecha_alta_twitter;
            this.ContAmigos = ContAmigos;
            this.ContSeguidores = ContSeguidores;
            this.descripcion = descripcion;
            this.lengua = lengua;
            this.location = location;
        }

    }
}
