﻿using System;
namespace RocProyect.Models
{
    public class Coordenadas
    {
		public double x { get; set; }
		public double y { get; set; }

        public Coordenadas(double x,double y)
        {
			this.x = x;
			this.y = y;
        }
    }
}
