﻿using System;
namespace RocProyect.Models
{
    public class UserApp
    {
       
        public int IDUsuario { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }//la tengo que guarda en md5
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }

        public string Twitter_ID { get; set; } //dejo el campo vacio

        //claves para conectarse a la app
        public string ConsumerKey { get; set; }
        public string ConsumerKeyApi { get; set; }
        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }

        public UserApp() { }

        public UserApp(string user, string contra, 
            string consumer1, string consumer2,
            string token1, string token2)
        {
            this.Usuario = user;
            this.Contrasena = contra;

            this.ConsumerKey = consumer1;
            this.ConsumerKeyApi = consumer2;
            this.AccessToken = token1;
            this.AccessTokenSecret = token2;
            var temp1 = token1;
            string[] key = temp1.Split('-');
            this.Twitter_ID = key[0];
        }

        public UserApp(int IDUsuario, string user, string contra, string consumer1,
            string consumer2, string token1, string token2)
        {
            this.IDUsuario = IDUsuario;
            this.Usuario = user;
            this.Contrasena = contra;
            this.ConsumerKey = consumer1;
            this.ConsumerKeyApi = consumer2;
            this.AccessToken = token1;
            this.AccessTokenSecret = token2;
            var temp1 = token1;
            string[] key = temp1.Split('-');
            this.Twitter_ID = key[0];
        }
    }
}
