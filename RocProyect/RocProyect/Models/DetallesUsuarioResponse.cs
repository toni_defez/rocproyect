﻿using System;
namespace RocProyect.Models
{
    public class DetallesUsuarioResponse
    {
        public string _id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string consumerKey { get; set; }
        public string consumerSecret { get; set; }
        public string accesToken { get; set; }
        public string accesTokenSecret { get; set; }
        public int __v { get; set; }
    }
}
