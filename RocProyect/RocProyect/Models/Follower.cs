﻿using System;
namespace RocProyect.Models
{
    public class Follower
    {
        public long id_follower;
		public int retweets;
		public string lengua;
		public string pais;

		public String Nombre { get; set; }
        public String Imagen { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public int ContSeguidores { get; set; }
        public int ContFav { get; set; }
        public string estado { get; set; }
        public bool following { get; set; } //para saber si me esta siguiendo el follower
        public int ContFriends { get; set; }
		public int ContTweets { get; set; }

		public Follower()
        {

        }
        public Follower(String nombre, String Imagen, DateTime fecha_creacion,
            int ContSeguidores)
        {
            this.Nombre = nombre;
            this.Imagen = Imagen;
            this.Fecha_Creacion = Fecha_Creacion;
            this.ContSeguidores = ContSeguidores;
        }
    }
}
