﻿using System;
namespace RocProyect.Models
{
    public class Tendencia
    {

		private string nombre;
		private int? volumen;

		public Tendencia()
		{
		}

		public string Nombre { get => nombre; set => nombre = value; }
		public int? Volumen { get => volumen; set => volumen = value; }
	}
}
