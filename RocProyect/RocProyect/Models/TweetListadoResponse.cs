﻿using System;
using System.Collections.Generic;

namespace RocProyect.Models
{
    public class TweetListadoResponse:OkResponse
    {
        public List<NodeTweet> tweet { get; set; }   
    }
}
