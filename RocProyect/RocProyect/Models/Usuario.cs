﻿using System;
namespace RocProyect.Models
{
    public class Usuario
    {
		private string nickname;
        private string profileUrl;
        public Usuario()
        {
        }

		public string Nickname { get => nickname; set => nickname = value; }
		public string ProfileUrl { get => profileUrl; set => profileUrl = value; }
	}
}
