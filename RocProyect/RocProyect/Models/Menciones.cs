﻿using System;
namespace RocProyect.Models
{
    public class Menciones
    {
        public int idmenciones { get; set; }
        public int IDUsuario { get; set; }

        public DateTime fecha { get; set; }
        public String fecha_corta { get { return fecha.ToString(); } }
        public string foto_usuario { get; set; }
        public string color_fondo { get; set; }
        public string nombre { get; set; }
        public string text { get; set; }
        public long TweetID { get; set; }

        /*referencia para ver si la mencion ha sido favorita,retweetado,contestada*/
        public bool favorito { get; set; }
        public bool retweet { get; set; }
        public bool contestado { get; set; }
        /*Para guardar las imagenes que nos interesan*/
        public string url_megusta;
        public string url_retweet;
        public string url_contestado;

        public Menciones()
        { }
    }
}
