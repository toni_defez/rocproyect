﻿using System;
using System.Collections.Generic;

namespace RocProyect.Models
{
    public class MiTweet
    {
        public long id { get; set; } //original.Id
        public string text { get; set; } //original.Text.Replace(",", "")
        public string time { get; set; } //original.CreatedAt
        public int retweets { get; set; }//original.RetweetCount
        public int favourites { get; set; }//original.FavouriteCount
        public int quote { get; set; }//original.quote_count
        public string nombre_usuario { get; set; }//original.User.Id
        public string imagen_usuario { get; set; }

        public string imagen_tweet { get; set; }
        public List<string> list_imagenes { get; set; }

        public bool favorito { get; set; }
        public bool retweet { get; set; }
        public bool contestado { get; set; }
		public long usuario { get; set; }


        public MiTweet() { }
        public MiTweet(long id, string text, string time, int retweets,
            int favourites, int quote, string nombre_usuario, string imagen_usuario)
        {
            this.id = id;
            this.text = text;
            this.time = time;
            this.retweets = retweets;
            this.favourites = favourites;
            this.quote = quote;
            this.nombre_usuario = nombre_usuario;
            this.imagen_usuario = imagen_usuario;
        }

        public int CompareTo(object obj)
        {
            MiTweet p = (MiTweet)obj;
            int resultado = this.id.CompareTo(p.id);
            if (resultado == 0)
            {
                return 0;
            }
            else
            {
                return resultado;
            }
        }
    }
}
