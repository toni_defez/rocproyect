﻿using System;
using System.Collections.Generic;

namespace RocProyect.Models
{
    public class Chat
    {
		private string _ultimoMensaje;
		private string _fotoUsuario;
		private string _nombreUsuario;
		private DateTime _fecha;
		private List<Mensaje> _conversacion;
		private long _idUsuario;
        
       
		public Chat(){}
		public Chat(long idUsuario)
		{
			IdUsuario = idUsuario;
		}
      
		public string UltimoMensaje { get => _ultimoMensaje; set => _ultimoMensaje = value; }
		public string FotoUsuario { get => _fotoUsuario; set => _fotoUsuario = value; }
		public string NombreUsuario { get => _nombreUsuario; set => _nombreUsuario = value; }
		public DateTime Fecha { get => _fecha; set => _fecha = value; }
		public List<Mensaje> Conversacion { get => _conversacion; set => _conversacion = value; }
		public long IdUsuario { get => _idUsuario; set => _idUsuario = value; }
	}
}
