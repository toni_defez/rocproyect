﻿using System;
namespace RocProyect.Models
{
    public class NodeTweet
    {
        public int __v { get; set; }
        public string text { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string autor { get; set; }
        public string _id { get; set; }
    }
}
