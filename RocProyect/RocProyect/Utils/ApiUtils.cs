﻿using System;
namespace RocProyect.Utils
{
    public class ApiUtils
    {
        //APIS PARA LOS USUARIOS
        public static string GetApiRegister()
        {
            return Constants.API_URL_DEV_REGISTER;
        }

        public static string GetApiLogin()
        {
            return Constants.API_URL_DEV_LOGIN;
        }

        public static string GetApiDetailsUser()
        {
            return Constants.API_URL_DEV_USER_DETAILS;
        }

        public static string GetApiDeleteUser(){
            return Constants.API_URL_DEV_BORRAR_USER;
        }


        public static string GetApiUpdateUser(){
            return Constants.API_URL_DEV_UPDATE_USER;
        }

		public static string GetApiRecuperarPass(){
			return Constants.API_URL_DEV_REMEMBER_PASS;
		}


        //APIS PARA LOS TWEETS

        public static string GetApiNewTweet()
        {
            return Constants.API_URL_DEV_CREAR_TWEET;
        }

        public static string GetApiUpdateTweet()
        {
            return Constants.API_URL_DEV_UPDATE_TWEET;
        }

        public static string GetApiListadoTweetUser()
        {
            return Constants.API_URL_DEV_OBTENER_LISTADO_TWEET;
        }

        public static string GetApiDeleteTweet()
        {
            return Constants.API_URL_DEV_DELETE_TWEET;
        }

        //PARA LAS NOTIFIACIONES
		public static string GetInitPush()
		{
			return Constants.API_URL_DEV_INICIAR_PUSH;
		}
    }
}
