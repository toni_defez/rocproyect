﻿using System;
using RocProyect.Models;
using Plugin.Geolocator;

namespace RocProyect.Utils
{
	public class GeoUtils
	{
		public static async System.Threading.Tasks.Task<Coordenadas> ObtenerCoordenadasAsync()
		{
			var locator = CrossGeolocator.Current;

            var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

			return new Coordenadas(position.Latitude, position.Longitude);
            
		}
	}
}
