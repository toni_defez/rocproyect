﻿using System;
namespace RocProyect.Utils
{
    public static class DateUtils
    {
        ///<summary>
        ///Devuelve el día de la semana Lunes Martes...
        ///</summary>
        /// <param name="fecha">Fecha</param>
        public static string GetDayOfTheWeek(DayOfWeek fecha)
        {
            if (fecha == DayOfWeek.Monday)
                return "Lunes";
            if (fecha == DayOfWeek.Tuesday)
                return "Martes";
            if (fecha == DayOfWeek.Wednesday)
                return "Miércoles";
            if (fecha == DayOfWeek.Thursday)
                return "Jueves";
            if (fecha == DayOfWeek.Friday)
                return "Viernes";
            if (fecha == DayOfWeek.Saturday)
                return "Sábado";
            else
                return "Domingo";
        }

        ///<summary>
        ///Devuelve el día en formato DD: 01, 02, ... 31
        ///</summary>
        /// <param name="fecha">Fecha</param>
        public static string GetDayNumber(DateTime fecha)
        {
            if (fecha.Day < 10) return "0" + fecha.Day;
            else return fecha.Day.ToString();
        }

        /// <summary>
        /// Devuelve el mes Enero Febrero...
        /// </summary>
        /// <param name="fecha">Fecha</param>
        public static string GetMonthString(DateTime fecha)
        {
            if (fecha.Month == 1) return "Enero";
            if (fecha.Month == 2) return "Febrero";
            if (fecha.Month == 3) return "Marzo";
            if (fecha.Month == 4) return "Abril";
            if (fecha.Month == 5) return "Mayo";
            if (fecha.Month == 6) return "Junio";
            if (fecha.Month == 7) return "Julio";
            if (fecha.Month == 8) return "Agosto";
            if (fecha.Month == 9) return "Septiembre";
            if (fecha.Month == 10) return "Octubre";
            if (fecha.Month == 11) return "Noviembre";
            else return "Diciembre";
        }

        /// <summary>
        /// Devuelve el día y el mes en formato 01 ENE
        /// </summary>
        /// <returns>The day and month.</returns>
        /// <param name="fecha">Fecha.</param>
        public static string GetDayAndMonth(DateTime fecha)
        {
            return GetDayNumber(fecha) + " " + GetMonthString(fecha).Substring(0, 3).ToUpper();
        }


        /// <summary>
        /// Devuelve el mes + el año: Enero 2018
        /// </summary>
        /// <param name="fecha">Fecha</param>
        public static string GetMonthAndYear(DateTime fecha)
        {
            return GetMonthString(fecha) + " " + fecha.Year;
        }

        public static string GetDateForNews(DateTime fecha)
        {
            return fecha.Day + " de " + GetMonthString(fecha);
        }

        ///<summary>
        ///Devuelve un string de la fecha en formato yyyy-MM-dd
        ///</summary>
        /// <param name="date">Fecha</param>
        public static string ObtenerFechaStringConGuiones(DateTime date)
        {
            string day, month;

            if (date.Day < 10) day = "0" + date.Day;
            else day = date.Day.ToString();

            if (date.Month < 10) month = "0" + date.Month;
            else month = date.Month.ToString();

            return (date.Year + "-" + month + "-" + day);
        }

        public static DateTime GetDateTime(string date)
        {
            DateTime fechaTime = DateTime.ParseExact(date, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            return fechaTime;
        }
    }
}
