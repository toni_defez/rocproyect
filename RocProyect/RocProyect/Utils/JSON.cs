﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using RocProyect.Models;

namespace RocProyect.Utils
{
    public class JSON
    {
        public static string SerializeJSON(Object data)
        {
            JsonSerializerSettings jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            var result = JsonConvert.SerializeObject(data,
                                                     Newtonsoft.Json.Formatting.None,
                                                     jsonSettings);
            return result;
        }

        public static object DeserializeJSON(string data)
        {
            return JsonConvert.DeserializeObject(data);
        }

		public static MiTweet DeserializeMiTweetJSON(string data)
        {
            return JsonConvert.DeserializeObject<MiTweet>(data);
        }

        public static Follower DeserializeFollowerJSON(string data)
        {
            return JsonConvert.DeserializeObject<Follower>(data);
        }

        public static Chat DeserializeChatJSON(string data)
		{
			return JsonConvert.DeserializeObject<Chat>(data);
		}

        public static UserNode DeserializeUsuarioLoginJSON(string v)
        {
			return JsonConvert.DeserializeObject<UserNode>(v);
        }



    }
}
