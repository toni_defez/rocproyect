﻿using System;
using System.Threading.Tasks;
using RocProyect.Models;

namespace RocProyect.Services
{
    public interface ITweetService
    {
        /// <summary>
        /// Creamos un nuevo tweet
        /// </summary>
        /// <returns>The new tweet async.</returns>
        /// <param name="tokenPass">Token pass.</param>
        /// <param name="text">Text.</param>
        /// <param name="fecha">Fecha.</param>
        /// <param name="hora">Hora.</param>
        Task<TweetActionResponse> GetNewTweetAsync(string tokenPass, string text,
                                                           string fecha,string hora);
        /// <summary>
        /// Borramos un tweet programado
        /// </summary>
        /// <returns>The delete tweet async.</returns>
        /// <param name="tokenPass">Token pass.</param>
        /// <param name="id">Identifier.</param>
        Task<OkResponse> GetDeleteTweetAsync(string tokenPass, string id);

        /// <summary>
        /// Editamos un tweet programado
        /// </summary>
        /// <returns>The update tweet async.</returns>
        /// <param name="tokenPass">Token pass.</param>
        /// <param name="text">Text.</param>
        /// <param name="fecha">Fecha.</param>
        /// <param name="hora">Hora.</param>
        /// <param name="id">Identifier.</param>
        Task<TweetActionResponse> GetUpdateTweetAsync(string tokenPass, string text,
                                                          string fecha, string hora,string id);

        /// <summary>
        /// Obtenemos todo los tweets programados de un usuario
        /// </summary>
        /// <returns>The listado tweet user async.</returns>
        /// <param name="tokenPass">Token pass.</param>
        Task<TweetListadoResponse> GetListadoTweetUserAsync(string tokenPass);
    }
}
