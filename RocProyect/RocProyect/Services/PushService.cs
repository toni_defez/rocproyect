﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RocProyect.Utils;

namespace RocProyect.Services
{
	public class PushService : IPushService
    {
		public PushService()
		{
		}

		public HttpClient BaseClient
        {
            get
            {
                var httpClient = new HttpClient();
                return httpClient;
            }
        }

		public async Task<bool> InitPush(string tokenPass)
		{
			Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",tokenPass },
				{"playerId",Constants.PLAYER_ID}
            };


            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
				response = BaseClient.PutAsync(ApiUtils.GetInitPush(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(json)) return false;
                if (!json.Contains("ok\":false"))
                {
                         return true;
                }
                return false;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return false;
            }

		}
	}
}
