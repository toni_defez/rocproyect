﻿using System;
using System.Threading.Tasks;

namespace RocProyect.Services
{
    public interface IPushService
    {
		/// <summary>
        /// Iniciamos el servicio push con el cliente
        /// </summary>
        /// <returns>The push.</returns>
        /// <param name="tokenPass">Token pass.</param>
		Task<bool> InitPush(string tokenPass);
    }
}
