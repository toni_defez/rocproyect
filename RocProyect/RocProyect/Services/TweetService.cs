﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Services
{
    public class TweetService : ITweetService
    {
        public HttpClient BaseClient
        {
            get
            {
                var httpClient = new HttpClient();
                return httpClient;
            }
        }


        public async Task<OkResponse> GetDeleteTweetAsync(string tokenPass, string id)
        {
            Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",tokenPass },
                {"id",id}
            };


            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
				response = BaseClient.PostAsync(ApiUtils.GetApiDeleteTweet(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                {
                    OkResponse okTweet =
                        JsonConvert.DeserializeObject<OkResponse>(json);

                    return okTweet;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }


        }

        public async Task<TweetListadoResponse> GetListadoTweetUserAsync(string tokenPass)
        {
            Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",tokenPass }
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                response = BaseClient.PostAsync(ApiUtils.GetApiListadoTweetUser(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                {
                    TweetListadoResponse listadoTweet =
                        JsonConvert.DeserializeObject<TweetListadoResponse>(json);

                    return listadoTweet;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }
        }

        public async Task<TweetActionResponse> GetNewTweetAsync(string tokenPass, string text, string fecha, string hora)
        {
            Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",tokenPass },
                {"text",text },
                {"fecha",fecha},
                {"hora",hora}
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                response = BaseClient.PostAsync(ApiUtils.GetApiNewTweet(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
              
                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                {
                    TweetActionResponse detallesUsuario =
                        JsonConvert.DeserializeObject<TweetActionResponse>(json);
                  
                    return detallesUsuario;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }
        }

        public async Task<TweetActionResponse> GetUpdateTweetAsync(string tokenPass, 
		                                                           string text, string fecha, string hora, string id)
        {
            Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",tokenPass },
                {"text",text },
                {"fecha",fecha},
                {"hora",hora},
				{"id",id}
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                response = BaseClient.PutAsync(ApiUtils.GetApiUpdateTweet(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                {
                    TweetActionResponse detallesUsuario =
                        JsonConvert.DeserializeObject<TweetActionResponse>(json);

                    return detallesUsuario;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }
        }
    }
}
