﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RocProyect.Models;
using RocProyect.Utils;

namespace RocProyect.Services
{
    public class UserService : IUserService
    {
		public UserService()
		{
		}

		public HttpClient BaseClient{
            get
            {
                var httpClient = new HttpClient();
                return httpClient;
            }
        }

        public async Task<OkResponse> GetDeleteUserAsync(string tokenPass, string id)
        {
            Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",tokenPass },
                {"id",id}
            };


            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                response = BaseClient.PostAsync(ApiUtils.GetApiDeleteTweet(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();

                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                {
                    OkResponse okTweet =
                        JsonConvert.DeserializeObject<OkResponse>(json);

                    return okTweet;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }


        }

        public async Task<EditarUsuarioResponse> GetEditarUsuarioResponseAsync(string email,
		                                                                       string password,
		                                                                       string tokenPass)
        {
            Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",tokenPass },
                {"email",email },
                {"password",password},
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                response = BaseClient.PostAsync(ApiUtils.GetApiUpdateUser(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                Debug.WriteLine(json);
                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                {
                    EditarUsuarioResponse detallesUsuario =
                        JsonConvert.DeserializeObject<EditarUsuarioResponse>(json);
                    return detallesUsuario;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }
        }

		public async Task<bool> GetRecuperarContra(string email)
		{
			Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"email",email }
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
				response = BaseClient.PostAsync(ApiUtils.GetApiRecuperarPass(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                Debug.WriteLine(json);
                if (string.IsNullOrEmpty(json)) return false;
                if (!json.Contains("ok\":false"))
                {
					OkResponse okResponse =
						JsonConvert.DeserializeObject<OkResponse>(json);
                    
					return okResponse.ok;
                }
                return false;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return false;
            }
		}

		public async Task<DetallesUsuarioResponse> GetResponseDetallesUsuarioAsync(string _tokenPass, string _id)
        {
            Dictionary<String, String> values = new Dictionary<string, string>
            {
                {"tokenPass",_tokenPass },
                {"id",_id }
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                response = BaseClient.PostAsync(ApiUtils.GetApiDetailsUser(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                Debug.WriteLine(json);
                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                { 
                    DetallesUsuarioResponse detallesUsuario =
                        JsonConvert.DeserializeObject<DetallesUsuarioResponse>(json);
                    Constants.KeyUsuario = detallesUsuario;
                    return detallesUsuario;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }
        }

        public async Task<LoginResponse> GetResponseLoginUserAsync(string email, string password)
        {
            var values = new Dictionary<String, String> {
                {"email",email},
                {"password",password},
            };

            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = new HttpResponseMessage();

            try
            {
                //poniendole header al HTTP client
                // string _ContentType = "application/json";
                //BaseClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));

                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));

                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                //_body.Headers.ContentType = new MediaTypeWithQualityHeaderValue(_ContentType);

                response = BaseClient.PostAsync(ApiUtils.GetApiLogin(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(json)) return null;
                if (!json.Contains("ok\":false"))
                {
                    LoginResponse reponseLogin = JsonConvert.DeserializeObject<LoginResponse>(json);
                    return reponseLogin;
                }
                return null;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return null;
            }
        }

        public async  Task<bool> GetResponseRegisterUser(string _email, string _password)
        {
            Dictionary<String, String> values = new Dictionary<String, String> {
                {"email",_email},
                {"password",_password },
				{"consumerKey",Constants.CONSUMER_KEY },
				{"consumerSecret",Constants.CONSUMER_SECRET },
				{"accesToken",Constants.ACCES_TOKEN },
				{"accesTokenSecret",Constants.ACCES_SECRET_TOKE }
            };


            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(values));
                HttpContent _body = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                response = BaseClient.PostAsync(ApiUtils.GetApiRegister(),
                                                      _body).Result;
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(json)) return false;
                if (!json.Contains("ok\":false"))
                {
                    OkResponse reponseOk = JsonConvert.DeserializeObject<OkResponse>(json);
                    return reponseOk.ok;
                }
                return false;
            }
            catch (Exception e)
            {
                var error = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(error);
                return false;
            }
        }

       
    }
}
