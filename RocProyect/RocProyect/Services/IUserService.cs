﻿using System;
using System.Threading.Tasks;
using RocProyect.Models;

namespace RocProyect.Services
{
	/// <summary>
    /// Interfaz para los servicios enlazado al usuario
    /// </summary>
	public interface IUserService
	{
		/// <summary>
        /// Comprueba si el usuario ha sido logeado correcatmente
        /// </summary>
        /// <returns>The response login user async.</returns>
        /// <param name="email">Email.</param>
        /// <param name="password">Password.</param>
		Task<LoginResponse> GetResponseLoginUserAsync(string email, string password);

        /// <summary>
        /// Realiza la operacion de registrar un nuevo usuario en el servidor
        /// </summary>
        /// <returns>The response register user.</returns>
        /// <param name="_email">Email.</param>
        /// <param name="_password">Password.</param>
		Task<bool> GetResponseRegisterUser(string _email, string _password);

        /// <summary>
        /// Obtenemos los datos necesarios para poder utilizar la libreria 
		/// de tweetInvi
        /// </summary>
        /// <returns>The response detalles usuario async.</returns>
        /// <param name="_tokenPass">Token pass.</param>
        /// <param name="_id">Identifier.</param>
		Task<DetallesUsuarioResponse> GetResponseDetallesUsuarioAsync(string _tokenPass, string _id);

        /// <summary>
        /// Borramos al usuario registrado en la base datos 
        /// </summary>
        /// <returns>The delete user async.</returns>
        /// <param name="tokenPass">Token pass.</param>
        /// <param name="id">Identifier.</param>
		Task<OkResponse> GetDeleteUserAsync(string tokenPass, string id);

        /// <summary>
        /// Editamos al usuario , unicamente poderemos modificar los campos 
		/// de contraseña y email
        /// </summary>
        /// <returns>The editar usuario response async.</returns>
        /// <param name="email">Email.</param>
        /// <param name="password">Password.</param>
        /// <param name="tokenPass">Token pass.</param>
		Task<EditarUsuarioResponse> GetEditarUsuarioResponseAsync(string email, string password, string tokenPass);

        /// <summary>
		/// Servicio que enviara una peticion al servidor para que
		/// envie un correo al email especificado
		/// </summary>
        /// <returns>The recuperar contra.</returns>
        /// <param name="email">Email.</param>
		Task<bool> GetRecuperarContra(string email);
	}
}
