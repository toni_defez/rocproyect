﻿using System;
using System.Collections.Generic;
using RocProyect.Models;

namespace RocProyect
{
	public class Constants
	{
		//API USUARIOS
		public static string API_URL_DEV_REGISTER = "http://twitterapp.ovh:5656/register";
		public static string API_URL_DEV_LOGIN = "http://twitterapp.ovh:5656/login";
		public static string API_URL_DEV_USER_DETAILS = "http://twitterapp.ovh:5656/userDetails";
		public static string API_URL_DEV_BORRAR_USER = "http://twitterapp.ovh:5656/userDelete";
		public static string API_URL_DEV_UPDATE_USER = "http://twitterapp.ovh:5656/user";
		public static string API_URL_DEV_REMEMBER_PASS = "http://twitterapp.ovh:5656/recuperarPassword";


		//API Tweet
		public static string API_URL_DEV_CREAR_TWEET = "http://twitterapp.ovh:5656/tweet";
		public static string API_URL_DEV_UPDATE_TWEET = "http://twitterapp.ovh:5656/tweet";
		public static string API_URL_DEV_DELETE_TWEET = "http://twitterapp.ovh:5656/tweetDelete";
		public static string API_URL_DEV_OBTENER_LISTADO_TWEET = "http://twitterapp.ovh:5656/userTweet";

		//para notificaciones
		public static string API_URL_DEV_INICIAR_PUSH = "http://twitterapp.ovh:5656/pushInit";


		//PARA LAS NOTIFICACIONES PUSH
		public static string PLAYER_ID;
		public static UserNode userNode;

		public static DetallesUsuarioResponse KeyUsuario { get; set; }


		//tokens para la comunicacion con Twitter;
		public static string CONSUMER_KEY = "FIagYsQNtkbYWmvHcxTB2owUy";
		public static string CONSUMER_SECRET = "dPRlKf20pTG1TkhNDuL1w6MlGB1yBH97atzAA0TitTK4AOnHBa";
		public static string ACCES_TOKEN { get; set; }
		public static string ACCES_SECRET_TOKE { get; set; }
              
        //datos para el logeo de twitter
		public static string NOMBRELOGIN { get; set; }
              
		//listas para la aplicacion      
		public static List<MiTweet> LIST_TWEET_TIMELINE { get; set; }
		public static List<Follower> LIST_FOLLOWERS { get; set; }
		public static List<Chat> LIST_CHATS { get; set; }

		public string key { get; set; }


		//Ubicacion España
		public static long idEspaña = 23424950;

		public static double LATITUD_ESPANA = 38.35280309139128;
		public static double LONGITUD_ESPANA = -0.47489674824615224;


		//claves para OneSignal
		public static string KEY_ONESIGNAL = "d7bb668d-55a2-447d-b179-e3475cee1e09";

	}
}
